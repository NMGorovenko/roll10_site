from django.urls import path, include
from . import views, ajax


# принимается функция home 
# из views в текущем каталоге
urlpatterns = [
    path('/register', views.RegisterViews, name='register'),
    path('/login/log', ajax.ajax_login, name='login'),
    path('/login', views.LoginViews, name='login'),
    path('/check_username/', ajax.check_username, name='check_username'),
    path('/Logout', views.LogoutUser, name='logout'),
]