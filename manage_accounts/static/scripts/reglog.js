// Регистрация
$(document).ready(function(){
    var eneble_register_login = false;
    var eneble_register_pass = false;
    $("#login_reg").on('keyup',
        function(){
            $.ajax({
                type: "GET",
                url: "check_username/",
                data:{
                    "username": $("#login_reg").val(),
                },
                dataType: "text",
                cache: false,
                success: function (data) {
                    if (data == 'yes'){
                        console.log(data);
                        $("#check_login").css("color", "#25D500");
                        $("#check_login").text("Логин свободен");
                        eneble_register_login = true;
                    }
                    else if (data == 'no'){
                        console.log(data); 
                        $("#check_login").css("color", "red");
                        $("#check_login").text("Логин занят");
                        eneble_register_login = false;
                        $("button[type='submit']").attr('disabled',true);
                    }
                    
                    if(eneble_register_login == true && eneble_register_pass == true){
                        $("button[type='submit']").attr('disabled',false);
                    }
                    else{
                        $("button[type='submit']").attr('disabled',true);
                    }
                }
            });
        }
    )
    $("#password2").on('keyup', function(){
            var value_input = $("#password1").val();
            var value_input2 = $(this).val();
            if (value_input != value_input2){
                $("#check_pass").css("color", "red");
                $("#check_pass").text("Пароли не совпадают!");
                eneble_register_pass = false;
            }
            else if (value_input == value_input2){
                $("#check_pass").css("color", "#25D500");
                $("#check_pass").text("Пароли совпадают");
                eneble_register_pass = true;
            }
            if(eneble_register_login == true && eneble_register_pass == true){
                $("button[type='submit']").attr('disabled',false);
            }
            else{
                $("button[type='submit']").attr('disabled',true);
            }
        }
    )
});



$(document).ready(function(){
    var csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
    console.log(csrf_token);
    $("#login_site").on('click',
        function(){
            $.ajax({
                type: "POST",
                url: "login/log",
                data: { 
                        "username": $("#login").val(),
                        "password": $("#password").val(),
 
                        "csrfmiddlewaretoken": csrf_token, 
                      },
                dataType: "text",
                cache: false,
                success: function (data) {
                    if (data == 'success'){
                        window.location.href = "/";
                    }
                    else if (data == 'error'){
                        $("#error_login").css("color", "red");
                        $("#error_login").text("Введен неверный логин или пароль");     
                    }
                }
            });
        }
    )

});