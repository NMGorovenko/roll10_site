from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.models import User, auth
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError

def RegisterViews(request):
    if request.method == 'POST':
        username = request.POST['login']
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        if (password1 == password2):
            try:
                user = User.objects.create_user(username, password=password1)
                user.save()
                user = auth.authenticate(username=username, password=password1)
                auth.login(request, user)
                return redirect('/')     
            except IntegrityError:
                return render(request, 'manage_accounts/register.html')   
        else:
            return render(request, 'manage_accounts/register.html')
    else:
        return render(request, 'manage_accounts/register.html')

    

def LoginViews(request):
    return render(request, 'manage_accounts/login.html')


def LogoutUser(request):
    auth.logout(request)
    return redirect("/")
    

