from django.http import HttpResponse
from django.contrib.auth.models import User, auth
from django.views.decorators.csrf import csrf_exempt
import json

# проверка логина при регистрации
def check_username(request):    
    if request.method == 'GET':
        username = request.GET['username']
        if username == "":
            return HttpResponse("no", content_type='text/html')
        names = None
        try:
            names = User.objects.get(username=username)
        except User.DoesNotExist:
            pass
        
        if names:
            return HttpResponse("no", content_type='text/html')
        else:
            return HttpResponse("yes", content_type='text/html')
                
    else:
        pass


def ajax_login(request):
    if request.method == 'POST':
        username = request.POST.get("username", None)
        password = request.POST.get("password", None)
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return HttpResponse("success", content_type='text/html')
        else:
            return HttpResponse("error", content_type='text/html')
    else:
        pass