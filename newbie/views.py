from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required 
from django.contrib.auth.models import User


def NewbieViews(request):
    return render(request, 'newbie/newbie.html')
