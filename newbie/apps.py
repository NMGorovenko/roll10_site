from django.apps import AppConfig


class NewbieConfig(AppConfig):
    name = 'newbie'
