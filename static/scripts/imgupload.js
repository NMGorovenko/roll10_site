//update_character/id
function GetCharUrl(){
    let cur_url = window.location.href;
    var number_of_border;

    // Определение границы '/' сслыки после которой идет id персонажа
    for (let i = cur_url.length - 1; cur_url[i] != '/'; i--){
        number_of_border = i;
    }

    var number_of_char = cur_url[number_of_border];

    // Определение номера персонажа
    for (let i = number_of_border + 1; i < cur_url.length; i++){
        number_of_char += cur_url[i];
    }

    return number_of_char;
};

var number_of_curchar = GetCharUrl();


$(document).ready(function(){
    $('#avatar').on('click', function(){
        $('#input-avatar').click();
    })

    var obj_size_mbyte = 3;
    $('#input-avatar').on('change', function() {
        if (this.files[0].type != 'image/jpeg' || this.files[0].size > obj_size_mbyte * 1024 * 1024) {
            this.files[0].value ="";
            Swal.fire({
                title: "Неподходящий файл!",
                text: "Загружаемый файл должен быть изображением" +
                      " расширения jpg, размер которого не превышает 3 Мбайт",
                icon: "error",
                showConfirmButton: false,
            });
        }
        else {
            console.log($('#input-avatar')[0].files[0]);
            var csrftoken = $('input[name="csrfmiddlewaretoken"]').attr('value');

            var img_data = $('#input-avatar')[0].files[0];
            var form_data = new FormData;
            //добавляем полученные данные о файле в FormData, первый аргумент('img') 
            //это ключ ассоциативного массива, по которому на бэкэнде получим данные по файлу.
            form_data.append("img", img_data);
            form_data.append("csrfmiddlewaretoken", csrftoken);
            console.log(form_data);

            $.ajax({
                type: 'POST',
                url: `save_avatar/${number_of_curchar}`,
                data: form_data,
                processData: false,
                contentType: false,
                dataType: 'text',
                cache: false,
                success: function(data){
                    if (data == "success"){
                        console.log("Успешное обновление картинки");
                        $.ajax({
                            method: "GET",
                            url: `ajax_return_character/${number_of_curchar}`,
                            dataType: "json",
                            data: {text: "text"},
                            success: function(data){
                                $('img').remove();
                                $('a#avatar').append(`<img src="${data.avatar_character}" 
                                                    style="width:500px; height:500px; 
                                                    border-radius: 10px;" alt="Загрузить картинку">`);
                            }
                        })
                    }
                    else if (data == "error"){
                        console.log("Безуспешная отправка картинки");
                    }
                },
                error: function(data){
                    console.log("Не удалось отправить данные на сервер");
                }
            });
            Swal.fire({
                title: "Загруженно корректное изображение",
                icon: "success",
                showConfirmButton: false,
            });
        }
    });
})