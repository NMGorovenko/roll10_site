function GetCharUrl(){
    let cur_url = window.location.href;
    var number_of_border;

    // Определение границы '/' сслыки после которой идет id персонажа
    for (let i = cur_url.length - 1; cur_url[i] != '/'; i--){
        number_of_border = i;
    }

    var number_of_char = cur_url[number_of_border];

    // Определение номера персонажа
    for (let i = number_of_border + 1; i < cur_url.length; i++){
        number_of_char += cur_url[i];
    }

    return number_of_char;
};

var number_of_curchar = GetCharUrl();


$(document).ready(function(){
    // Кибернетика
    $('button[id="add_cyber"]').on('click', function(){
        $.ajax({
            method: "GET",
            url: `ajax_add_cybernetic/${number_of_curchar}`,
            dataType: "json",
            data: {text: "text"},
            success: function(data){
                last_of_tab = Object.keys(data).pop();
                $('table[id="cybernetic"]').append(`<tr><td><input class="table_long_input" name="type_cyber${last_of_tab}"
                                                   value=""></td><td><input class="short_input" name="cyber_hl${last_of_tab}"
                                                   type="number" value="0" min="0" max="100" step="1"></td><td><input class="short_input"
                                                   name="cyber_cost${last_of_tab}" type="number" value="0" min="0" max="100" step="1"></td></tr>`);
                $('tr[id="hat"]').appendTo($('table[id="cybernetic"]'));
                console.log("строка кибернетики добавлена");
                }
                })
    })
    //Событий жини
    $('button[id="add_life"]').on('click', function(){
        $.ajax({
        method: "GET",
        url: `ajax_add_life_events/${number_of_curchar}`,
        dataType: "json",
        data: {text: "text"},
        success: function(data){
            last_of_tab = Object.keys(data).pop();
            $('table[id="life_path"]').append(`<tr><td><input class="year_input" value="0" name="event${last_of_tab}"
                                              type="number"></td><td><input class="table_long_input" value="0" 
                                              name="event_year${last_of_tab}"></td></tr>`);
            console.log("строка события жизни добавлена");
            }
            })
    })
    //Снаряжение
    $('button[id="add_gear"]').on('click', function(){
            $.ajax({
            method: "GET",
            url: `ajax_add_gears/${number_of_curchar}`,
            dataType: "json",
            data: {text: "text"},
            success: function(data){
                last_of_tab = Object.keys(data).pop();
                $('table[id="gear"]').append(`<tr><td><input class="table_long_input" name="type_gear${last_of_tab}"
                                             value=""></td><td><input class="short_input" name="gear_hl${last_of_tab}"
                                             type="number" value="0" min="0" max="100" step="1"></td><td><input
                                             class="short_input" name="gear_cost${last_of_tab}" type="number"
                                             value="0" min="0" max="100" step="1"></td></tr>`);
                console.log("строка снаряжения добавлена");
            }
            })
    })
    // Оружие
    $('button[id="add_weapon"]').on('click', function(){
        $.ajax({
        method: "GET",
        url: `ajax_add_weapons/${number_of_curchar}`,
        dataType: "json",
        data: {text: "text"},
        success: function(data){
            last_of_tab = Object.keys(data).pop();
            $('table[id="weapon"]').append(`<tr><td><input name="weapon_name${last_of_tab}" class="table_long_input"
                                           value=""></td><td><input name="type_weapon${last_of_tab}" class="short_input"
                                           type="text" value=""></td><td><input name="accuracy${last_of_tab}" class="short_input"
                                           type="number" value="" min="0" max="100" step="1"></td><td><input name="conc${last_of_tab}"
                                           class="short_input" type="number" value="" min="0" max="100" step="1"></td><td><input 
                                           name="avall${last_of_tab}" class="short_input" type="number" value="" min="0" max="100"
                                           step="1"></td><td><input name="dam${last_of_tab}" class="short_input" type="number" value=""
                                           min="0" max="100" step="1"></td><td><input name="shells${last_of_tab}" class="short_input"
                                           type="number" value="" min="0" max="100" step="1"></td><td><input name="pace${last_of_tab}"
                                           class="short_input" type="number" value="" min="0" max="100" step="1"></td><td><input 
                                           name="reliability${last_of_tab}" class="short_input" type="number"  value="" min="0"
                                           max="100" step="1"></td></tr>`);
            console.log("строка оружия добавлена");
            }
        })
    })
})
