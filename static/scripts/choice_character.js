$(document).ready(function(){
    $.ajax({
        type: "GET",
        url: "player_profile/get_character",
        data:{
            "username": $("#login_reg").val(),
        },
        dataType: "json",
        cache: false,
        success: function (data) {
            if (data){
                console.log(data);
                var i = 1;
                for (let key in data){
                    console.log(data);
                    $('.conteiner').append(`<a class="character" href="/characters/player_profile/${key}">
                                            <span class="elem_character" id="number">${i}</span>
                                            <span class="elem_character" id="picture">
                                            <img src="${data[key]["avatar"]}" width="200" height="200" alt="картинка">
                                            </span>
                                            <span class="elem_character" id="nickname">${data[key]["nickname"]}</span>
                                            <span class="elem_character" id="role">${data[key]["role"]}</span>
                                            <div class="stats">
                                                <span class="elem_character" id="rep">Репутация: ${data[key]["reputation"]}</span>
                                                <span class="elem_character" id="score">Очки улуч: ${data[key]["score_upgrade"]}</span>
                                                <span class="elem_character" id="hum">Чел: ${data[key]["Humanity"]}</span>
                                            </div>  
                                            </a>`);
                    i++;
                }
            }
            else if (!data){
                console.log("error"); 

            }
        }
    });

});