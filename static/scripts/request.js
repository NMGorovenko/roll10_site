//ajax_return_character/ - общий путь JSON'ов
//window.location.href

// Получение id выбранного персонажа
function GetCharUrl(){
    let cur_url = window.location.href;
    var number_of_border;

    // Определение границы '/' сслыки после которой идет id персонажа
    for (let i = cur_url.length - 1; cur_url[i] != '/'; i--){
        number_of_border = i;
    }

    var number_of_char = cur_url[number_of_border];

    // Определение номера персонажа
    for (let i = number_of_border + 1; i < cur_url.length; i++){
        number_of_char += cur_url[i];
    }

    return number_of_char;
};

var number_of_curchar = GetCharUrl();

// Загрузка данных из JSON в html
$(document).ready(async function DataToPage() {
    var request_chars = await fetch(`ajax_return_character/${number_of_curchar}`);
    if (request_chars.ok){
        var text_chars = await request_chars.json();

        // Основные элементы описания перса:
        var name = document.getElementsByName("nickname");

        name.value = text_chars.name;

        var reputation = document.getElementsByName("reputation");
        reputation.value = text_chars.reputation;

        var upgrade_score = document.getElementsByName("score_upgrade");
        upgrade_score.value = text_chars.score_upgrade;

        var humanity = document.getElementsByName("humanity");
        humanity.value = text_chars.Humanity;


        // Статы:
        var intellect = document.getElementsByName("intellect");
        intellect.value = text_chars.intellect;

        var reflex = document.getElementsByName("reflex");
        reflex.value = text_chars.reflex;

        var tech_abilities = document.getElementsByName("tech");
        tech_abilities.value = text_chars.tech;

        var cool = document.getElementsByName("cool");    // Хладнокровие
        cool.value = text_chars.cool;

        var attraction = document.getElementsByName("attr");    // Привлекательность
        attraction.value = text_chars.attr;

        var luck = document.getElementsByName("luck");
        luck.value = text_chars.luck;

        var speed = document.getElementsByName("ma");
        speed.value = text_chars.ma;

        var body_type = document.getElementsByName("body");
        body_type.value = text_chars.body;

        var empathy = document.getElementsByName("emp");    // Эмпатия
        empathy.value = text_chars.emp;

        var run = document.getElementsByName("run");
        run.value = text_chars.run;

        var leap = document.getElementsByName("leap");    // Прыжок
        leap.value = text_chars.leap;

        var lift = document.getElementsByName("lift");    // Подъем
        lift.value = text_chars.lift;


        // Специальные умения:
        var authority = document.getElementsByName("authority"); // Авторитет
        authority.value = text_chars.authority;

        var charismatic_leadership = document.getElementsByName("charismatic_leadership"); // Харизматичное лидерство
        charismatic_leadership.value = text_chars.charismatic_leadership;

        var battle_sence = document.getElementsByName("battle_sence");
        battle_sence.value = text_chars.battle_sence;

        var credibllity = document.getElementsByName("credibllity");
        credibllity.value = text_chars.credibllity;

        var family = document.getElementsByName("family");
        family.value = text_chars.family;

        var interface = document.getElementsByName("interface");
        interface.value = text_chars.interface;

        var emergency_repair = document.getElementsByName("emergency_repair");
        emergency_repair.value = text_chars.emergency_repair;

        var medicine = document.getElementsByName("medicine");
        medicine.value = text_chars.medicine;

        var resources = document.getElementsByName("resources");
        resources.value = text_chars.resources;

        var pull = document.getElementsByName("pull");
        pull.value = text_chars.pull;

        var pull = document.getElementsByName("pull");
        pull.value = text_chars.pull;


        // Привлекательность:
        var personal_grooming = document.getElementsByName("personal_grooming");
        personal_grooming.value = text_chars.personal_grooming;

        var wardrobe_and_style = document.getElementsByName("wardrobe_and_style");
        wardrobe_and_style.value = text_chars.wardrobe_and_style;


        // Зависит от Рефлексов:
        var bow_crossbow = document.getElementsByName("bow_crossbow");
        bow_crossbow.value = text_chars.bow_crossbow;

        var athletics = document.getElementsByName("athletics");
        athletics.value = text_chars.athletics;

        var fight = document.getElementsByName("fight");
        fight.value = text_chars.fight;

        var dance = document.getElementsByName("dance");
        dance.value = text_chars.dance;

        var evasion = document.getElementsByName("evasion");
        evasion.value = text_chars.evasion;

        var driving = document.getElementsByName("driving");
        driving.value = text_chars.driving;

        var fencing = document.getElementsByName("fencing");
        fencing.value = text_chars.fencing;

        var pistol = document.getElementsByName("pistol");
        pistol.value = text_chars.pistol;

        var heavy_weapon = document.getElementsByName("heavy_weapon");
        heavy_weapon.value = text_chars.heavy_weapon;

        var melee = document.getElementsByName("melee");
        melee.value = text_chars.melee;

        var motorcycle_control = document.getElementsByName("motorcycle_control");
        motorcycle_control.value = text_chars.motorcycle_control;

        var transport_management = document.getElementsByName("transport_management");
        transport_management.value = text_chars.transport_management;

        var helicopter_piloting = document.getElementsByName("helicopter_piloting");
        helicopter_piloting.value = text_chars.helicopter_piloting;

        var airplane_piloting = document.getElementsByName("airplane_piloting");
        airplane_piloting.value = text_chars.airplane_piloting;

        var airship_piloting = document.getElementsByName("airship_piloting");
        airship_piloting.value = text_chars.airship_piloting;

        var av_aerodine_piloting = document.getElementsByName("av_aerodine_piloting");
        av_aerodine_piloting.value = text_chars.av_aerodine_piloting;

        var gun = document.getElementsByName("gun");
        gun.value = text_chars.gun;

        var stealth = document.getElementsByName("stealth");
        stealth.value = text_chars.stealth;

        var submachine_gun = document.getElementsByName("submachine_gun");
        submachine_gun.value = text_chars.submachine_gun;


        // Тип тела:
        var endurance = document.getElementsByName("endurance");
        endurance.value = text_chars.endurance;

        var demo_strengh = document.getElementsByName("demo_strengh");
        demo_strengh.value = text_chars.demo_strengh;

        var swimming = document.getElementsByName("swimming");
        swimming.value = text_chars.swimming;


        // Зависят от эмпатии:
        var human_understanding = document.getElementsByName("human_understanding");
        human_understanding.value = text_chars.human_understanding;

        var interview = document.getElementsByName("interview");
        interview.value = text_chars.interview;

        var leadership = document.getElementsByName("leadership");
        leadership.value = text_chars.leadership;

        var seduction = document.getElementsByName("seduction");
        seduction.value = text_chars.seduction;

        var good_manners = document.getElementsByName("good_manners");
        good_manners.value = text_chars.good_manners;

        var conviction = document.getElementsByName("conviction");
        conviction.value = text_chars.conviction;

        var public_performance = document.getElementsByName("public_performance");
        public_performance.value = text_chars.public_performance;


        // Зависят от интелекта:
        var accounting = document.getElementsByName("accounting");
        accounting.value = text_chars.accounting;

        var anthropology = document.getElementsByName("anthropology");
        anthropology.value = text_chars.anthropology;

        var awareness_notice = document.getElementsByName("awareness_Notice");
        awareness_notice.value = text_chars.awareness_notice;

        var biology = document.getElementsByName("biology");
        biology.value = text_chars.biology;

        var botany = document.getElementsByName("botany");
        botany.value = text_chars.botany;

        var chemistry = document.getElementsByName("chemistry");
        chemistry.value = text_chars.chemistry;

        var composition = document.getElementsByName("composition");
        composition.value = text_chars.composition;

        var diagnose_illness = document.getElementsByName("diagnose_illness");
        diagnose_illness.value = text_chars.diagnose_illness;

        var education_and_Gen_Know = document.getElementsByName("education_and_Gen_Know");
        education_and_Gen_Know.value = text_chars.education_and_Gen_Know;

        var expert = document.getElementsByName("expert");
        expert.value = text_chars.expert;

        var games_of_chance = document.getElementsByName("games_of_chance");
        games_of_chance.value = text_chars.games_of_chance;

        var geology = document.getElementsByName("geology");
        geology.value = text_chars.geology;

        var hide_evade = document.getElementsByName("hide_evade");
        hide_evade.value = text_chars.hide_evade;

        var history = document.getElementsByName("history");
        history.value = text_chars.history;

        var use_of_libraries = document.getElementsByName("use_of_libraries");
        use_of_libraries.value = text_chars.use_of_libraries;

        var mathematics = document.getElementsByName("mathematics");
        mathematics.value = text_chars.mathematics;

        var physics = document.getElementsByName("physics");
        physics.value = text_chars.physics;

        var programming = document.getElementsByName("programming");
        programming.value = text_chars.programming;

        var tracking = document.getElementsByName("tracking");
        tracking.value = text_chars.tracking;

        var gambling = document.getElementsByName("gambling");
        gambling.value = text_chars.gambling;

        var network_knowledge = document.getElementsByName("network_knowledge");
        network_knowledge.value = text_chars.network_knowledge;

        var teaching = document.getElementsByName("teaching");
        teaching.value = text_chars.teaching;

        var survival = document.getElementsByName("survival");
        survival.value = text_chars.survival;

        var zoology = document.getElementsByName("zoology");
        zoology.value = text_chars.zoology;


        // Зависят от Хладнокровия:
        var interrogation = document.getElementsByName("interrogation");
        interrogation.value = text_chars.interrogation;

        var intimidate = document.getElementsByName("intimidate");
        intimidate.value = text_chars.intimidate;

        var oratory = document.getElementsByName("oratory");
        oratory.value = text_chars.oratory;

        var resist_torture_drugs = document.getElementsByName("resist_torture_drugs");
        resist_torture_drugs.value = text_chars.resist_torture_drugs;

        var streetwise = document.getElementsByName("streetwise");
        streetwise.value = text_chars.streetwise;


        // Зависят от технических способностей:
        var technology_airplane = document.getElementsByName("technology_airplane");
        technology_airplane.value = text_chars.technology_airplane;

        var technology_av_aerodine = document.getElementsByName("technology_av_aerodine");
        technology_av_aerodine.value = text_chars.technology_av_aerodine;

        var basic_technology = document.getElementsByName("basic_technology");
        basic_technology.value = text_chars.basic_technology;

        var cryocamera_control = document.getElementsByName("cryocamera_control");
        cryocamera_control.value = text_chars.cryocamera_control;

        var technology_cyberdeck = document.getElementsByName("technology_cyberdeck");
        technology_cyberdeck.value = text_chars.technology_cyberdeck;

        var cyber_technology = document.getElementsByName("cyber_technology");
        cyber_technology.value = text_chars.cyber_technology;

        var explosives = document.getElementsByName("explosives");
        explosives.value = text_chars.explosives;

        var disguise = document.getElementsByName("disguise");
        disguise.value = text_chars.disguise;

        var electronics = document.getElementsByName("electronics");
        electronics.value = text_chars.electronics;

        var electronic_security = document.getElementsByName("electronic_security");
        electronic_security.value = text_chars.electronic_security;

        var first_aid = document.getElementsByName("first_aid");
        first_aid.value = text_chars.first_aid;

        var fake = document.getElementsByName("fake");
        fake.value = text_chars.fake;

        var technology_helicopter = document.getElementsByName("technology_helicopter");
        technology_helicopter.value = text_chars.technology_helicopter;

        var painting = document.getElementsByName("painting");
        painting.value = text_chars.painting;

        var cinema_photography = document.getElementsByName("cinema_photography");
        cinema_photography.value = text_chars.cinema_photography;

        var pharmaceuticals = document.getElementsByName("pharmaceuticals");
        pharmaceuticals.value = text_chars.pharmaceuticals;

        var opening_locks = document.getElementsByName("opening_locks");
        opening_locks.value = text_chars.opening_locks;

        var steal = document.getElementsByName("steal");
        steal.value = text_chars.steal;

        var playing_instrument = document.getElementsByName("playing_instrument");
        playing_instrument.value = text_chars.playing_instrument;

        var technology_weapon = document.getElementsByName("technology_weapon");
        technology_weapon.value = text_chars.technology_weapon;

        $(`#${text_chars.role}`).attr('selected');
    }
    else {
        console.log("Ошибка запроса по статам: " + request_chars.status);
    }


    //ajax_return_cybernetic/id - общая для кибернетики
    /*var request_cybernetic = await fetch(`ajax_return_cybernetic/${number_of_curchar}`);
    if (request_cybernetic.ok){
        var text_cybernetic = await request_cybernetic.json();
        for (let key in text_cybernetic){

            for (let k in text_cybernetic[key]){

                alert(text_cybernetic[key][k]);
                //let elem = document.getElementById(`${k}${key}`);
                //elem.value = text_cybernetic[`${k}${key}`];

            }
        }
    }
    else {
        console.log("Ошибка запроса по кибернетике: " + request_cybernetic.status);
    }*/


    //ajax_return_life_path/id - общая для событий в жизни
    /*var request_life_path = await fetch(`ajax_return_life_path/${number_of_curchar}`);
    if (request_life_path.ok){
        var text_life_path = await request_life_path.json();
        for (let key in text_life_path){

            for (let k in text_life_path[key]){

                alert(text_life_path[key][k]);
                //let elem = document.getElementById(`${k}${key}`);
                //elem.value = text_life_path[`${k}${key}`];

            }
        }
    }
    else {
        console.log("Ошибка запроса по жизненному пути: " + request_life_path.status);
    }*/


    //ajax_return_gears/id - общая для снаряжения
    /*var request_gears = await fetch(`ajax_return_gears/${number_of_curchar}`);
    if (request_gears.ok){
        var text_gears = await request_gears.json();
        for (let key in text_gears){

            for (let k in text_gears[key]){

                alert(text_gears[key][k]);
                //let elem = document.getElementById(`${k}${key}`);
                //elem.value = text_gears[`${k}${key}`];

            }
        }
    }
    else {
        console.log("Ошибка запроса по снаряжению: " + request_gears.status);
    }*/


    //ajax_return_weapons/id - общая для снаряжения
    /*var request_weapons = await fetch(`ajax_return_weapons/${number_of_curchar}`);
    if (request_weapons.ok){
        var text_weapons = await request_weapons.json();
        for (let key in text_weapons){

            for (let k in text_weapons[key]){

                alert(text_weapons[key][k]);
                //let elem = document.getElementById(`${k}${key}`);
                //elem.value = text_weapons[`${k}${key}`];

            }
        }
    }
    else {
        console.log("Ошибка запроса по оружию: " + request_weapons.status);
    }*/

});
