$(document).ready(function(){
    $("#roll_cool").on('click', function(){
        var val_cool = Number($('#cool').val());
        var interrogation_val = Number($('#interrogation').val());
        var intimidate_val = Number($('#intimidate').val());
        var oratory_val = Number($('#oratory').val());
        var resist_torture_drugs_val = Number($('#resist_torture_drugs').val());
        var streetwise_val = Number($('#streetwise').val());

        var cool_abil = {
            "Допрос": interrogation_val,
            "Угрожать/запугивать": intimidate_val,
            "Красноречие": oratory_val,
            "Устойчивость к наркоте/пыткам": resist_torture_drugs_val,
            "Знание улиц": streetwise_val,
        };

        Swal.fire({
            html: `<div id='modal_abil_cool'>
                  <div>
                  <h3>Прокидывание</h3>
                  <h4>Хладнокровие</h4>
                  <br><br>
                  <p>Выберите способность:</p>
                  <select id='abil_cool'>
                  <option name='abillity_cool'>Допрос</option>
                  <option name='abillity_cool'>Угрожать/запугивать</option>
                  <option name='abillity_cool'>Красноречие</option>
                  <option name='abillity_cool'>Устойчивость к наркоте/пыткам</option>
                  <option name='abillity_cool'>Знание улиц</option>
                  </select>
                  </div>
                  <br><br>
                  <button id='roll_abil_cool'>Кинуть кости</button>
                  </div>`,
                  showConfirmButton: false,
        });
        $('#roll_abil_cool').on('click', function(){

            var selected_abil = $('#abil_cool').val();
            for (let k in cool_abil){

                if (k == selected_abil){
                    var cur_abil = cool_abil[k];
                    break;
                }
            }

            let dice = Math.floor((Math.random() * 11) % 10 + 1);

            $('#rolled_cool').remove();
            let roll = dice + val_cool + cur_abil;
            $('#modal_abil_cool').append(`<p id='rolled_cool'>Хладнокровие (${val_cool}) +
              ${selected_abil} (${cur_abil}) + Бросок костей (${dice}) = ${roll}</p>`);
        })
    })
})
