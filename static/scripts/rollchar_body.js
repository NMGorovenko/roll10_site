$(document).ready(function(){
    $("#roll_body").on('click', function(){
        var val_body = Number($('#body').val());
        var endurance_val = Number($('#endurance').val());
        var demo_strengh_val = Number($('#demo_strengh').val());
        var swimming_val = Number($('#swimming').val());

        var body_abil = {
            "Выносливость": endurance_val,
            "Проявление силы": demo_strengh_val,
            "Плавание": swimming_val,
        };

        Swal.fire({
            html: `<div id='modal_abil_body'>
                  <div>
                  <h3>Прокидывание</h3>
                  <h4>Тип тела</h4>
                  <br><br>
                  <p>Выберите способность:</p>
                  <select id='abil_body'>
                  <option name='abillity_body'>Выносливость</option>
                  <option name='abillity_body'>Проявление силы</option>
                  <option name='abillity_body'>Плавание</option>
                  </select>
                  </div>
                  <br><br>
                  <button id='roll_abil_body'>Кинуть кости</button>
                  </div>`,
                  showConfirmButton: false,
        });
        $('#roll_abil_body').on('click', function(){

            var selected_abil = $('#abil_body').val();
            for (let k in body_abil){

                if (k == selected_abil){
                    var cur_abil = body_abil[k];
                    break;
                }
            }

            let dice = Math.floor((Math.random() * 11) % 10 + 1);

            $('#rolled_body').remove();
            let roll = dice + val_body + cur_abil;
            $('#modal_abil_body').append(`<p id='rolled_body'>Тип тела (${val_body}) +
              ${selected_abil} (${cur_abil}) + Бросок костей (${dice}) = ${roll}</p>`);
        })
    })
})
