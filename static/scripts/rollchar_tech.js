$(document).ready(function(){
    $("#roll_tech").on('click', function(){
        var val_tech = Number($('#tech').val());
        var technology_airplane_val = Number($('#technology_airplane').val());
        var technology_av_aerodine_val = Number($('#technology_av_aerodine').val());
        var basic_technology_val = Number($('#basic_technology').val());
        var cryocamera_control_val = Number($('#cryocamera_control').val());
        var technology_cyberdeck_val = Number($('#technology_cyberdeck').val());
        var cyber_technology_val = Number($('#cyber_technology').val());
        var explosives_val = Number($('#explosives').val());
        var disguise_val = Number($('#disguise').val());
        var electronics_val = Number($('#electronics').val());
        var electronic_security_val = Number($('#electronic_security').val());
        var first_aid_val = Number($('#first_aid').val());
        var fake_val = Number($('#fake').val());
        var technology_helicopter_val = Number($('#technology_helicopter').val());
        var painting_val = Number($('#painting').val());
        var cinema_photography_val = Number($('#cinema_photography').val());
        var pharmaceuticals_val = Number($('#pharmaceuticals').val());
        var opening_locks_val = Number($('#opening_locks').val());
        var steal_val = Number($('#steal').val());
        var playing_instrument_val = Number($('#playing_instrument').val());
        var technology_weapon_val = Number($('#technology_weapon').val());

        var tech_abil = {
            "Технология(самолет)": technology_airplane_val,
            "Технология(AV/Аэродайн)": technology_av_aerodine_val,
            "Базовая технология": basic_technology_val,
            "Управление криокамерой": cryocamera_control_val,
            "Технология(кибердека)": technology_cyberdeck_val,
            "Кибертехнология": cyber_technology_val,
            "Взрывчатка": explosives_val,
            "Маскировка": disguise_val,
            "Электроника": electronics_val,
            "Электронная безопасность": electronic_security_val,
            "Первая помощь": first_aid_val,
            "Подделка": fake_val,
            "Технология(Вертолет)": technology_helicopter_val,
            "Рисование": painting_val,
            "Кино-фотосъемка": cinema_photography_val,
            "Фармацевтика": pharmaceuticals_val,
            "Вскрытие замков": opening_locks_val,
            "Кража": steal_val,
            "Игра на инструменте":  playing_instrument_val,
            "Технология(Оружие)": technology_weapon_val,
        };

        Swal.fire({
            html: `<div id='modal_abil_tech'>
                  <div>
                  <h3>Прокидывание</h3>
                  <h4>Технические способности</h4>
                  <br><br>
                  <p>Выберите способность:</p>
                  <select id='abil_tech'>
                  <option name='abillity_tech'>Технология(самолет)</option>
                  <option name='abillity_tecg'>Технология(AV/Аэродайн)</option>
                  <option name='abillity_tech'>Базовая технология</option>
                  <option name='abillity_tech'>Управление криокамерой</option>
                  <option name='abillity_tech'>Технология(кибердека)</option>
                  <option name='abillity_tech'>Кибертехнология</option>
                  <option name='abillity_tech'>Взрывчатка</option>
                  <option name='abillity_tech'>Маскировка</option>
                  <option name='abillity_tech'>Электроника</option>
                  <option name='abillity_tech'>Электронная безопасность</option>
                  <option name='abillity_tech'>Первая помощь</option>
                  <option name='abillity_tech'>Подделка</option>
                  <option name='abillity_tech'>Технология(Вертолет)</option>
                  <option name='abillity_tech'>Рисование</option>
                  <option name='abillity_tech'>Кино-фотосъемка</option>
                  <option name='abillity_tech'>Фармацевтика</option>
                  <option name='abillity_tech'>Вскрытие замков</option>
                  <option name='abillity_tech'>Кража</option>
                  <option name='abillity_tech'>Игра на инструменте</option>
                  <option name='abillity_tech'>Технология(Оружие)</option>
                  </select>
                  </div>
                  <br><br>
                  <button id='roll_abil_tech'>Кинуть кости</button>
                  </div>`,
                  showConfirmButton: false,
        });
        $('#roll_abil_tech').on('click', function(){

            var selected_abil = $('#abil_tech').val();
            for (let k in tech_abil){

                if (k == selected_abil){
                    var cur_abil = tech_abil[k];
                    break;
                }
            }

            let dice = Math.floor((Math.random() * 11) % 10 + 1);

            $('#rolled_tech').remove();
            let roll = dice + val_tech + cur_abil;
            $('#modal_abil_tech').append(`<p id='rolled_tech'>Тех. способности (${val_tech}) +
              ${selected_abil} (${cur_abil}) + Бросок костей (${dice}) = ${roll}</p>`);
        })
    })
})
