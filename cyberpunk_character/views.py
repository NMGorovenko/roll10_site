from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import HttpResponseNotFound, JsonResponse
from .models import Character, Cybernetics, Life_events, Gear, Weapon
from . import ajax
from django.contrib.auth.decorators import login_required 
from django.core import serializers
import os
from django.conf import settings
from django.utils.datastructures import MultiValueDictKeyError
from .generate_character import Character as GeneratedCharacter

@login_required(login_url='/account/login')
def CharacterViews(request, id):
    num = id  
    if Character.objects.get(id = num).user_id != request.user:
        return HttpResponseNotFound("<h2>Тру хацкер?</h2>")
    else:

        character = Character.objects.get(id = num)
        
        
        # Кибернетика
        cybernetics_list = Cybernetics.objects.filter(character_id = num)
        lost_humanity = 0
        for cybernetic in cybernetics_list:
            lost_humanity += cybernetic.hl

        # События в жизни
        life_events = Life_events.objects.filter(character_id = num)
            
        # Шмотки
        gears = Gear.objects.filter(character_id = num)

        # Оружие
        weapons = Weapon.objects.filter(character_id = num)


        # аватар

        return render(request, 'cyberpunk_character/player.html', {#"character": character,
                                                                   #"cybernetics_list": cybernetics_list,
                                                                   #"cybernetic_hl_total": lost_humanity,
                                                                   #"life_events": life_events,
                                                                   #"gears": gears,
                                                                   #"weapons": weapons
                                                                   })


@login_required(login_url='/account/login')
def Profile_playerViews(request):

    list_characters = Character.objects.filter(user_id = request.user.id)
    return render(request, 'main_app/profile_player.html', {"list_characters": list_characters})


@login_required(login_url='/account/login')
def UpdateCharacterViews(request, id):
    if request.method == 'POST':
        num = id
        character = Character.objects.get(id = num)
    
        character.name = request.POST['nickname']
        character.reputation = request.POST['reputation']
        character.score_upgrade = request.POST['score_upgrade']
        character.Humanity = request.POST['Humanity']

        character.intellect = request.POST['intellect']
        character.reflex = request.POST['reflex']
        character.tech = request.POST['tech']
        character.cool = request.POST['cool']
        character.attr = request.POST['attr']
        character.luck = request.POST['luck']
        character.ma = request.POST['ma']
        character.body = request.POST['body']
        character.emp = request.POST['emp']
        character.run = request.POST['run']
        character.leap = request.POST['leap']
        character.lift = request.POST['lift']
        #character.role.choises = request.POST['select_role']

        # аватарка
        try:
            character.avatar_character = request.FILES[f"avatar{num}"]
        except MultiValueDictKeyError:
            pass
        

        # Кибернетика
        cybernetics_list = Cybernetics.objects.filter(character_id = num)

        for cyber in cybernetics_list:
            ident = cyber.id
            cyber.type_cyber = request.POST[f'type_cyber{ident}']
            cyber.hl = request.POST[f'cyber_hl{ident}']
            cyber.cost = request.POST[f'cyber_cost{ident}']
            cyber.save()


        # События в жизни
        life_events = Life_events.objects.filter(character_id = num)
        for life_event in life_events:
            ident = life_event.id
            life_event.year = request.POST[f'event_year{ident}']
            life_event.event = request.POST[f'event{ident}']
            life_event.save()

        # Шмотки
        gears = Gear.objects.filter(character_id = num)
        for gear in gears:
            ident = gear.id
            gear.type_gear = request.POST[f'type_gear{ident}']
            gear.hl = request.POST[f'gear_hl{ident}']
            gear.cost = request.POST[f'gear_cost{ident}']
            gear.save()

        # Оружие
        weapons = Weapon.objects.filter(character_id = num)
        for weapon in weapons:
            ident = weapon.id

            weapon.name = request.POST[f'weapon_name{ident}']
            weapon.type_weapon = request.POST[f'type_weapon{ident}']
            weapon.accuracy = request.POST[f'accuracy{ident}']
            weapon.conc = request.POST[f'conc{ident}']
            weapon.avall = request.POST[f'avall{ident}']
            weapon.dam = request.POST[f'dam{ident}']
            weapon.shells = request.POST[f'shells{ident}']
            weapon.pace = request.POST[f'pace{ident}']
            weapon.reliability = request.POST[f'reliability{ident}']

            weapon.save()

        
        character.save()
        return redirect(f"/characters/player_profile/{num}")
    else:
        return HttpResponseNotFound("<h2>ОШИБочка</h2>")


@login_required(login_url='/account/login')
def CreateNewCharacter(request):
    character = Character(user_id=request.user)
    character.save()
    character_id = character.id
    new_character = GeneratedCharacter()
    new_character = new_character.chars
    
    # character.name = new_character['Роль']
    


    intellect_temp = new_character['Интеллект']
    reflex_temp = new_character['Рефлексы']
    tech_temp = new_character['Технологии']
    attr_temp = new_character['Привлекательность']
    emp_temp = new_character['Эмпатия']
    cool_temp = new_character['Хладнокровие']
    body_temp = new_character['Тип тела']
    
    

    

    # Парсим основные характеристики
    character.name = new_character['Кличка']
    character.role = new_character['Роль']
    character.reputation = new_character['rep']


    character.intellect = intellect_temp["Интеллект"][1]
    character.reflex = reflex_temp["Рефлексы"][1]
    character.tech = tech_temp["Технологии"][1]
    character.cool = new_character["Хладнокровие"]["Хладнокровие"][1]
    character.attr = attr_temp["Привлекательность"][1]
    character.luck = new_character['Удача']
    character.ma = new_character['Скорость']
    character.body = body_temp["Тип тела"][1]
    character.emp = emp_temp["Эмпатия"][1]
    character.ma = new_character['Скорость']
    

    character.run = character.ma * 3
    character.leap = character.ma * (3/4)
    character.lift = character.body * 40
    # Конец генерации основных характеристик
    
    # Парсим способности по интелекту
    character.accounting = 1
    character.anthropology = 1
    character.awareness_notice = 1
    character.biology = 1
    character.botany = 1
    character.chemistry = 1
    character.composition = 1
    character.diagnose_illness = 1
    character.education_and_Gen_Know = 1
    character.expert = 1
    character.games_of_chance = 1
    character.geology = 1
    character.hide_evade = 1
    character.history = 1
    character.use_of_libraries = 1
    character.mathematics = 1
    character.physics = 1
    character.programming = 1
    character.tracking = 1
    character.gambling = 1
    character.network_knowledge = 1
    character.teaching = 1
    character.survival = 1
    character.zoology = 1
    character.language_1_name = new_character["info"]['Язык']
    character.language_2_name = ""
    character.language_3_name = ""
    # парсим зависимости от рефлексов
    character.bow_crossbow = 1
    character.athletics = 1
    character.dance = 1
    character.evasion = 1
    character.driving = 1
    character.fencing = 1
    character.pistol = 1
    character.heavy_weapon = 1
    character.martial_arts_1 = 1
    character.melee = 1
    character.motorcycle_control = 1
    character.transport_management = 1
    character.helicopter_piloting = 1
    character.airplane_piloting = 1
    character.airship_piloting = 1
    character.av_aerodine_piloting = 1
    character.gun = 1
    character.stealth = 1
    character.submachine_gun = 1

    # тех способности
    character.technology_airplane = 1
    character.technology_av_aerodine = 1
    character.basic_technology = 1
    character.cryocamera_control = 1
    character.technology_cyberdeck = 1
    character.cyber_technology = 1
    character.explosives = 1
    character.disguise = 1
    character.electronics = 1
    character.electronic_security = 1
    character.first_aid = 1
    character.fake = 1
    character.technology_helicopter = 1
    character.painting = 1
    character.cinema_photography = 1
    character.pharmaceuticals = 1
    character.opening_locks = 1
    character.steal = 1
    character.playing_instrument = 1
    character.technology_weapon = 1

    # эмпатия
    character.human_understanding = 1
    character.interview = 1
    character.leadership = 1
    character.seduction = 1
    character.good_manners = 1
    character.conviction = 1
    character.public_performance = 1

    # тип тела
    character.endurance = 1
    character.demo_strengh = 1
    character.swimming = 1

    # хладнокровие
    character.interrogation = 1
    character.intimidate = 1
    character.oratory = 1
    character.resist_torture_drugs = 1
    character.streetwise = 1

    # привликательность
    character.personal_grooming = 1
    character.wardrobe_and_style = 1

    # Стиль
    character.clothing = new_character["info"]['Одежда']
    character.hair = new_character["info"]['Прическа']
    character.affectations = new_character["info"]['Фишка']
    character.ethnlcity = new_character["info"]['Происхождение']
    character.language = new_character["info"]['Язык']


    # Мотивация
    character.motivation = "Выжить"
    character.trait = new_character["info"]['Черта']
    character.value_most = new_character["info"]['Ценишь']
    character.attitude_to_people = new_character["info"]['Отношение']
    character.significant_subject = new_character["info"]['Значимый']

    # Братья и сестры
    character.brothers = new_character["info"]['Братья']
    character.sisters = new_character["info"]['Сестры']
    character.family_text = new_character['info']["Семья"]
    
    # Таблицы
    # События
    year = 16
    for i in new_character['info']["События"]:
        life_path = Life_events(character_id = character)
        life_path.year = year
        life_path.event = i
        life_path.save()
        year += 1

    # 
    

    character.save()
    return redirect(f"/characters/player_profile/{character.id}")


def ajax_return_character(request, id):
    num = id
    character = Character.objects.get(id = num)
    try:
        avatar_link = character.avatar_character.url
    except ValueError:
        avatar_link = "http://cdn.onlinewebfonts.com/svg/img_337315.png"

    json_character = {
                      # Основные определители персонажа:
                      "nickname": character.name,
                      "avatar_character": avatar_link,
                      "reputation": character.reputation,
                      "score_upgrade": character.score_upgrade,
                      "Humanity": character.Humanity,
                      "select_role": character.role,

                      # Статы:
                      "intellect": character.intellect,
                      "reflex": character.reflex,
                      "tech": character.tech,
                      "cool": character.cool,
                      "attr": character.attr,
                      "luck": character.luck,
                      "ma": character.ma,
                      "body": character.body,
                      "emp": character.emp,
                      "run": character.run,
                      "leap": character.leap,
                      "lift": character.lift,

                      # Специальные возможности:
                      "authority": character.authority,
                      "charismatic_leadership": character.charismatic_leadership,
                      "battle_sence": character.battle_sence,
                      "credibllity": character.credibllity,
                      "family": character.family,
                      "interface": character.interface,
                      "emergency_repair": character.emergency_repair,
                      "medicine": character.medicine,
                      "resources": character.resources,
                      "pull": character.pull,

                      # Зависимость от интелекта:
                      "accounting": character.accounting,
                      "anthropology": character.anthropology,
                      "awareness_notice": character.awareness_notice,
                      "biology": character.biology,
                      "botany": character.botany,
                      "chemistry": character.chemistry,
                      "composition": character.composition,
                      "diagnose_illness": character.diagnose_illness,
                      "education_and_Gen_Know": character.education_and_Gen_Know,
                      "expert": character.expert,
                      "games_of_chance": character.games_of_chance,
                      "geology": character.geology,
                      "hide_evade": character.hide_evade,
                      "history": character.history,
                      "language_1": character.language_1,
                      "language_1_name": character.language_1_name,
                      "language_2": character.language_2,
                      "language_2_name": character.language_2_name,
                      "language_3": character.language_3,
                      "language_3_name": character.language_3_name,
                      "use_of_libraries": character.use_of_libraries,
                      "mathematics": character.mathematics,
                      "physics": character.physics,
                      "programming": character.programming,
                      "tracking": character.tracking,
                      "gambling": character.gambling,
                      "network_knowledge": character.network_knowledge,
                      "teaching": character.teaching,
                      "survival": character.survival,
                      "zoology": character.zoology,

                      # Внешний вид:
                      "personal_grooming": character.personal_grooming,
                      "wardrobe_and_style": character.wardrobe_and_style,

                      # Зависящие от физических характеристик:
                      "endurance": character.endurance,
                      "demo_strengh": character.demo_strengh,
                      "swimming": character.swimming,

                      # Крутость\дикость:
                      "interrogation": character.interrogation,
                      "intimidate": character.intimidate,
                      "oratory": character.oratory,
                      "resist_torture_drugs": character.resist_torture_drugs,
                      "streetwise": character.streetwise,

                      # Зависящие от эмпатии:
                      "human_understanding": character.human_understanding,
                      "interview": character.interview,
                      "leadership": character.leadership,
                      "seduction": character.seduction,
                      "good_manners": character.good_manners,
                      "conviction": character.conviction,
                      "public_performance": character.public_performance,

                      # Зависящие от рефлексов:
                      "bow_crossbow": character.bow_crossbow,
                      "athletics": character.athletics,
                      "fight": character.fight,
                      "dance": character.dance,
                      "evasion": character.evasion,
                      "driving": character.driving,
                      "fencing": character.fencing,
                      "pistol": character.pistol,
                      "heavy_weapon": character.heavy_weapon,
                      "martial_arts_1": character.martial_arts_1,
                      "martial_arts_1_name": character.martial_arts_1_name,
                      "martial_arts_2": character.martial_arts_2,
                      "martial_arts_2_name": character.martial_arts_2_name,
                      "martial_arts_3": character.martial_arts_3,
                      "martial_arts_3_name": character.martial_arts_3_name,
                      "melee": character.melee,
                      "motorcycle_control": character.motorcycle_control,
                      "transport_management": character.transport_management,
                      "helicopter_piloting": character.helicopter_piloting,
                      "airplane_piloting": character.airplane_piloting,
                      "airship_piloting": character.airship_piloting,
                      "av_aerodine_piloting": character.av_aerodine_piloting,
                      "gun": character.gun,
                      "stealth": character.stealth,
                      "submachine_gun": character.submachine_gun,

                      # Зависящие от тех. умений:
                      "technology_airplane": character.technology_airplane,
                      "technology_av_aerodine": character.technology_av_aerodine,
                      "basic_technology": character.basic_technology,
                      "cryocamera_control": character.cryocamera_control,
                      "technology_cyberdeck": character.technology_cyberdeck,
                      "cyber_technology": character.cyber_technology,
                      "explosives": character.explosives,
                      "disguise": character.disguise,
                      "electronics": character.electronics,
                      "electronic_security": character.electronic_security,
                      "first_aid": character.first_aid,
                      "fake": character.fake,
                      "technology_helicopter": character.technology_helicopter,
                      "painting": character.painting,
                      "cinema_photography": character.cinema_photography,
                      "pharmaceuticals": character.pharmaceuticals,
                      "opening_locks": character.opening_locks,
                      "steal": character.steal,
                      "playing_instrument": character.playing_instrument,
                      "technology_weapon": character.technology_weapon,

                      # Одежда:
                      "clothes": character.clothing,
                      "hairstyle": character.hair,
                      "chip": character.affectations,
                      "race": character.ethnlcity,
                      "language": character.language,

                      # Семья:
                      "family_text": character.family_text,
                      "brothers": character.brothers,
                      "sisters": character.sisters,

                      # Мотивация:
                      "motivation": character.motivation,
                      "trait": character.trait,
                      "value_most": character.value_most,
                      "attitude_to_people": character.attitude_to_people,
                      "significant_subject": character.significant_subject,
                      }

    return JsonResponse(json_character)

def ajax_return_cybernetic(request, id):
    num = id
    json_cybernetics = dict()
    cybernetics_list = Cybernetics.objects.filter(character_id = num)
    for cybernetic in cybernetics_list:
        json_cybernetics[f'{cybernetic.id}'] = {f'type_cyber{cybernetic.id}':cybernetic.type_cyber,
                                                f'cyber_hl{cybernetic.id}':cybernetic.hl,
                                                f'cyber_cost{cybernetic.id}':cybernetic.cost,}

    return JsonResponse(json_cybernetics)

def ajax_return_life_path(request, id):
    num = id
    json_life_path = dict()
    life_path_list = Life_events.objects.filter(character_id = num)
    for life_path in life_path_list:
        json_life_path[f'{life_path.id}'] = {f'event_year{life_path.id}':life_path.year,
                                              f'event{life_path.id}':life_path.event}

    return JsonResponse(json_life_path)

def ajax_return_gears(request, id):
    num = id
    json_gears = dict()
    gears_list = Gear.objects.filter(character_id = num)
    for gear in gears_list:
        json_gears[f'{gear.id}'] = {f'type_gear{gear.id}':gear.type_gear,
                                                f'gear_hl{gear.id}':gear.hl,
                                                f'gear_cost{gear.id}':gear.cost}

    return JsonResponse(json_gears)

def ajax_return_weapons(request, id):
    num = id
    json_weapons = dict()
    weapons_list = Weapon.objects.filter(character_id = num)
    for weapon in weapons_list:
        json_weapons[f'{weapon.id}'] = {f'weapon_name{weapon.id}': weapon.name,
                                                f'type_weapon{weapon.id}': weapon.type_weapon,
                                                f'accuracy{weapon.id}': weapon.accuracy,
                                                f'conc{weapon.id}': weapon.conc,
                                                f'avall{weapon.id}': weapon.avall,
                                                f'dam{weapon.id}': weapon.dam,
                                                f'shells{weapon.id}': weapon.shells,
                                                f'pace{weapon.id}': weapon.pace,
                                                f'reliability{weapon.id}': weapon.reliability}

    return JsonResponse(json_weapons)

def ajax_add_cybernetic(request, id):
    num = id
    character = Character.objects.get(id = num)
    json_cybernetics = dict()

    cybernetic = Cybernetics(character_id = character)
    
    cybernetic.save()
    json_cybernetics[f'{cybernetic.id}'] = {f'type_cyber{cybernetic.id}': cybernetic.type_cyber,
                                            f'hl{cybernetic.id}': cybernetic.hl,
                                            f'cost{cybernetic.id}': cybernetic.cost}
    return JsonResponse(json_cybernetics)

def ajax_add_life_events(request, id):
    num = id
    character = Character.objects.get(id = num)
    json_life_path = dict()
    life_path = Life_events(character_id = character)
    life_path.save()
    json_life_path[f'{life_path.id}'] = {f'event_year{life_path.id}': life_path.year,
                                              f'event{life_path.id}': life_path.event}
    return JsonResponse(json_life_path)
    
def ajax_add_gears(request, id):
    num = id
    character = Character.objects.get(id = num)
    json_gear = dict()

    gear = Gear(character_id = character)
    gear.save()
    json_gear[f'{gear.id}'] = {f'type_gear{gear.id}': gear.type_gear,
                                f'gear_hl{gear.id}': gear.hl,
                                f'gear_cost{gear.id}': gear.cost}
    return JsonResponse(json_gear)

def ajax_add_weapons(request, id):
    num = id
    character = Character.objects.get(id = num)
    json_weapons = dict()

    weapon = Weapon(character_id = character)
    weapon.save()
    json_weapons[f'{weapon.id}'] = {f'weapon_name{weapon.id}': weapon.name,
                                    f'type_weapon{weapon.id}': weapon.type_weapon,
                                    f'accuracy{weapon.id}': weapon.accuracy,
                                    f'conc{weapon.id}': weapon.conc,
                                    f'avall{weapon.id}': weapon.avall,
                                    f'dam{weapon.id}': weapon.dam,
                                    f'shells{weapon.id}': weapon.shells,
                                    f'pace{weapon.id}': weapon.pace,
                                    f'reliability{weapon.id}': weapon.reliability}
    return JsonResponse(json_weapons)