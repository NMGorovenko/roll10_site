# Generated by Django 3.0.5 on 2020-05-19 14:24

import cyberpunk_character.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cyberpunk_character', '0010_auto_20200510_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='character',
            name='avatar_character',
            field=models.ImageField(blank=True, null=True, upload_to=cyberpunk_character.models.path_and_rename, verbose_name='Аватарка'),
        ),
        migrations.AlterField(
            model_name='gear',
            name='type_gear',
            field=models.CharField(blank=True, default='', max_length=40, null=True, verbose_name='Тип снаряжения'),
        ),
        migrations.AlterField(
            model_name='weapon',
            name='name',
            field=models.CharField(blank=True, default='', max_length=40, null=True, verbose_name='Название'),
        ),
        migrations.AlterField(
            model_name='weapon',
            name='type_weapon',
            field=models.CharField(blank=True, default='', max_length=40, null=True, verbose_name='Тип'),
        ),
    ]
