# Generated by Django 3.0.5 on 2020-05-07 11:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cyberpunk_character', '0005_auto_20200506_1103'),
    ]

    operations = [
        migrations.AddField(
            model_name='character',
            name='attitude_to_people',
            field=models.CharField(default='', max_length=100, verbose_name='отношение к людям'),
        ),
        migrations.AddField(
            model_name='character',
            name='brothers',
            field=models.PositiveIntegerField(default=0, verbose_name='Братьев'),
        ),
        migrations.AddField(
            model_name='character',
            name='family_text',
            field=models.TextField(default='', max_length=100, verbose_name='Семья'),
        ),
        migrations.AddField(
            model_name='character',
            name='motivation',
            field=models.CharField(default='', max_length=100, verbose_name='мотивация'),
        ),
        migrations.AddField(
            model_name='character',
            name='significant_subject',
            field=models.CharField(default='', max_length=100, verbose_name='значимый предмет'),
        ),
        migrations.AddField(
            model_name='character',
            name='sisters',
            field=models.PositiveIntegerField(default=0, verbose_name='Сестер'),
        ),
        migrations.AddField(
            model_name='character',
            name='trait',
            field=models.CharField(default='', max_length=100, verbose_name='черта характера'),
        ),
        migrations.AddField(
            model_name='character',
            name='value_most',
            field=models.CharField(default='', max_length=100, verbose_name='ценишь больше всего'),
        ),
        migrations.AlterField(
            model_name='character',
            name='martial_arts_1_name',
            field=models.CharField(default='БИ 1', max_length=40, verbose_name='БИ 1'),
        ),
        migrations.AlterField(
            model_name='character',
            name='martial_arts_2_name',
            field=models.CharField(default='БИ 2', max_length=40, verbose_name='БИ 2'),
        ),
        migrations.AlterField(
            model_name='character',
            name='martial_arts_3_name',
            field=models.CharField(default='БИ 3', max_length=40, verbose_name='БИ 3'),
        ),
        migrations.CreateModel(
            name='Weapon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40, verbose_name='Название')),
                ('type_weapon', models.CharField(max_length=40, verbose_name='Тип')),
                ('accuracy', models.PositiveIntegerField(default=0, verbose_name='Точность')),
                ('conc', models.PositiveIntegerField(default=0, verbose_name='Скрыт')),
                ('avall', models.PositiveIntegerField(default=0, verbose_name='Дост')),
                ('dam', models.PositiveIntegerField(default=0, verbose_name='ПОВР')),
                ('shells', models.PositiveIntegerField(default=0, verbose_name='Снаряды')),
                ('pace', models.PositiveIntegerField(default=0, verbose_name='ТЕМП СТРЕЛ')),
                ('reliability', models.PositiveIntegerField(default=0, verbose_name='Надежность')),
                ('character_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cyberpunk_character.Character')),
            ],
        ),
        migrations.CreateModel(
            name='Life_events',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event', models.CharField(default='', max_length=40, verbose_name='событие жизни')),
                ('year', models.PositiveIntegerField(default=0, verbose_name='год')),
                ('character_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cyberpunk_character.Character')),
            ],
        ),
    ]
