from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import HttpResponseNotFound, JsonResponse, HttpResponse
from .models import Character
from .models import Cybernetics, Life_events, Gear, Weapon
from django.contrib.auth.decorators import login_required 
from django.core import serializers
import os
import json
from django.conf import settings
from django.utils.datastructures import MultiValueDictKeyError


@login_required(login_url='/account/login')
def get_character(request):
    list_characters = Character.objects.filter(user_id = request.user.id)
    json_data = dict()
    for character in list_characters:
        try:
            avatar_link = character.avatar_character.url
        except ValueError:
            avatar_link = "http://cdn.onlinewebfonts.com/svg/img_337315.png"
        json_data[f"{character.id}"] =  {"id": character.id,
                                         "nickname": character.name,
                                         "reputation": character.reputation,
                                         "score_upgrade": character.score_upgrade,
                                         "Humanity": character.Humanity,
                                         "role": character.role,
                                         "avatar": avatar_link}
    return JsonResponse(json_data)


@login_required(login_url='/account/login')
def ajax_save_character(request, id):
    if request.method == 'POST':
        id_character = id
        character = Character.objects.get(id = id_character)
        try:
            character = Character.objects.get(id = id_character)
        except:
            character = None
        if character is not None:
            character.name = request.POST.get("nickname", None)
            character.reputation = request.POST.get("reputation", None)
            character.score_upgrade = request.POST.get("score_upgrade", None)
            character.Humanity = request.POST.get("Humanity", None)
            character.role = request.POST.get("select_role", None)
            character.intellect = request.POST.get("intellect", None)
            character.reflex = request.POST.get("reflex", None)
            character.tech = request.POST.get("tech", None)
            character.cool = request.POST.get("cool", None)
            character.attr = request.POST.get("attr", None)
            character.luck = request.POST.get("luck", None)
            character.ma = request.POST.get("ma", None)
            character.body = request.POST.get("body", None)
            character.emp = request.POST.get("emp", None)
            character.run = request.POST.get("run", None)
            character.leap = request.POST.get("leap", None)
            character.lift = request.POST.get("lift", None)
            character.authority = request.POST.get("authority", None)
            character.charismatic_leadership = request.POST.get("charismatic_leadership", None)
            character.battle_sence = request.POST.get("battle_sence", None)
            character.credibllity = request.POST.get("credibllity", None)
            character.family = request.POST.get("family", None)
            character.interface = request.POST.get("interface", None)
            character.emergency_repair = request.POST.get("emergency_repair", None)
            character.medicine = request.POST.get("medicine", None)
            character.resources = request.POST.get("resources", None)
            character.pull = request.POST.get("pull", None)
            character.accounting = request.POST.get("accounting", None)
            character.anthropology = request.POST.get("anthropology", None)
            character.awareness_notice = request.POST.get("awareness_notice", None)
            character.biology = request.POST.get("biology", None)
            character.botany = request.POST.get("botany", None)
            character.chemistry = request.POST.get("chemistry", None)
            character.composition = request.POST.get("composition", None)
            character.diagnose_illness = request.POST.get("diagnose_illness", None)
            character.education_and_Gen_Know = request.POST.get("education_and_Gen_Know", None)
            character.expert = request.POST.get("expert", None)
            character.games_of_chance = request.POST.get("games_of_chance", None)
            character.geology = request.POST.get("geology", None)
            character.hide_evade = request.POST.get("hide_evade", None)
            character.history = request.POST.get("history", None)
            character.language_1 = request.POST.get("language_1", None)
            character.language_1_name = request.POST.get("language_1_name", None)
            character.language_2 = request.POST.get("language_2", None)
            character.language_2_name = request.POST.get("language_2_name", None)
            character.language_3 = request.POST.get("language_3", None)
            character.language_3_name = request.POST.get("language_3_name", None)
            character.use_of_libraries = request.POST.get("use_of_libraries", None)
            character.mathematics = request.POST.get("mathematics", None)
            character.physics = request.POST.get("physics", None)
            character.programming = request.POST.get("programming", None)
            character.tracking = request.POST.get("tracking", None)
            character.gambling = request.POST.get("gambling", None)
            character.network_knowledge = request.POST.get("network_knowledge", None)
            character.teaching = request.POST.get("teaching", None)
            character.survival = request.POST.get("survival", None)
            character.zoology = request.POST.get("zoology", None)
            character.personal_grooming = request.POST.get("personal_grooming", None)
            character.wardrobe_and_style = request.POST.get("wardrobe_and_style", None)
            character.endurance = request.POST.get("endurance", None)
            character.demo_strengh = request.POST.get("demo_strengh", None)
            character.swimming = request.POST.get("swimming", None)
            character.interrogation = request.POST.get("interrogation", None)
            character.intimidate = request.POST.get("intimidate", None)
            character.oratory = request.POST.get("oratory", None)
            character.resist_torture_drugs = request.POST.get("resist_torture_drugs", None)
            character.streetwise = request.POST.get("streetwise", None)
            character.human_understanding = request.POST.get("human_understanding", None)
            character.interview = request.POST.get("interview", None)
            character.leadership = request.POST.get("leadership", None)
            character.seduction = request.POST.get("seduction", None)
            character.good_manners = request.POST.get("good_manners", None)
            character.conviction = request.POST.get("conviction", None)
            character.public_performance = request.POST.get("public_performance", None)
            character.bow_crossbow = request.POST.get("bow_crossbow", None)
            character.athletics = request.POST.get("athletics", None)
            character.fight = request.POST.get("fight", None)
            character.dance = request.POST.get("dance", None)
            character.evasion = request.POST.get("evasion", None)
            character.driving = request.POST.get("driving", None)
            character.fencing = request.POST.get("fencing", None)
            character.pistol = request.POST.get("pistol", None)
            character.heavy_weapon = request.POST.get("heavy_weapon", None)

            character.martial_arts_1 = request.POST.get("martial_arts_1", None)
            character.martial_arts_1_name = request.POST.get("martial_arts_1_name", None)
            character.martial_arts_2 = request.POST.get("martial_arts_2", None)
            character.martial_arts_2_name = request.POST.get("martial_arts_2_name", None)
            character.martial_arts_3 = request.POST.get("martial_arts_3", None)
            character.martial_arts_3_name = request.POST.get("martial_arts_3_name", None)

            character.melee = request.POST.get("melee", None)
            character.motorcycle_control = request.POST.get("motorcycle_control", None)
            character.transport_management = request.POST.get("transport_management", None)
            character.helicopter_piloting = request.POST.get("helicopter_piloting", None)
            character.airplane_piloting = request.POST.get("airplane_piloting", None)
            character.airship_piloting = request.POST.get("airship_piloting", None)
            character.av_aerodine_piloting = request.POST.get("av_aerodine_piloting", None)
            character.gun = request.POST.get("gun", None)
            character.stealth = request.POST.get("stealth", None)
            character.submachine_gun = request.POST.get("submachine_gun", None)
            character.technology_airplane = request.POST.get("technology_airplane", None)
            character.technology_av_aerodine = request.POST.get("technology_av_aerodine", None)
            character.basic_technology = request.POST.get("basic_technology", None)
            character.cryocamera_control = request.POST.get("cryocamera_control", None)
            character.technology_cyberdeck = request.POST.get("technology_cyberdeck", None)
            character.cyber_technology = request.POST.get("cyber_technology", None)
            character.explosives = request.POST.get("explosives", None)
            character.disguise = request.POST.get("disguise", None)
            character.electronics = request.POST.get("electronics", None)
            character.electronic_security = request.POST.get("electronic_security", None)
            character.first_aid = request.POST.get("first_aid", None)
            character.fake = request.POST.get("fake", None)
            character.technology_helicopter = request.POST.get("technology_helicopter", None)
            character.painting = request.POST.get("painting", None)
            character.cinema_photography = request.POST.get("cinema_photography", None)
            character.pharmaceuticals = request.POST.get("pharmaceuticals", None)
            character.opening_locks = request.POST.get("opening_locks", None)
            character.steal = request.POST.get("steal", None)
            character.playing_instrument = request.POST.get("playing_instrument", None)
            character.technology_weapon = request.POST.get("technology_weapon", None)
            character.brothers = request.POST.get("brothers", None)
            character.sisters = request.POST.get("sisters", None)
            character.family_text = request.POST.get("family_text", None)
            
            character.clothing = request.POST.get("clothes", None)
            character.hair = request.POST.get("hairstyle", None)
            character.affectations = request.POST.get("chip", None)
            character.ethnlcity = request.POST.get("race", None)
            character.language = request.POST.get("language", None)

            character.motivation = request.POST.get("motivation", None)
            character.trait = request.POST.get("trait", None)
            character.value_most = request.POST.get("value_most", None)
            character.attitude_to_people = request.POST.get("attitude_to_people", None)
            character.significant_subject = request.POST.get("significant_subject", None)

            cyber = json.loads(request.POST.get("cyber", None))
            gears = json.loads(request.POST.get("gears", None))
            life_events = json.loads(request.POST.get("life_events", None))
            weapons = json.loads(request.POST.get("weapons", None))

            for key in cyber["cybernetics"].keys():
                try:
                    cybernetic = Cybernetics.objects.get(id = int(key))
                    cybernetic.type_cyber = cyber["cybernetics"][key]["type_cyber"]
                    cybernetic.hl = cyber["cybernetics"][key]["cyber_hl"]
                    cybernetic.cost = cyber["cybernetics"][key]["cyber_cost"]
                    cybernetic.save()
                except:
                    pass

            for key in life_events["life_events"].keys():
                try:
                    life_event = Life_events.objects.get(id = int(key))
                    life_event.year = life_events["life_events"][key]["event_year"]
                    life_event.event = life_events["life_events"][key]["event"]
                    life_event.save()
                except:
                    pass
            
            for key in gears["gears"].keys():
                try:
                    gear = Gear.objects.get(id = int(key))
                    gear.type_gear = gears["gears"][key]["type_gear"]
                    gear.hl = gears["gears"][key]["gear_hl"]
                    gear.cost = gears["gears"][key]["gear_cost"]
                    gear.save()
                except:
                    pass

            for key in weapons["weapons"].keys():
                print(weapons["weapons"][key])
                try:
                    weapon = Weapon.objects.get(id = int(key))
                    weapon.name = weapons["weapons"][key]["weapon_name"]
                    weapon.type_weapon = weapons["weapons"][key]["type_weapon"]
                    weapon.accuracy = weapons["weapons"][key]["accuracy"]
                    weapon.conc = weapons["weapons"][key]["conc"]
                    weapon.avall = weapons["weapons"][key]["avall"]
                    weapon.dam = weapons["weapons"][key]["dam"]
                    weapon.shells = weapons["weapons"][key]["shells"]
                    weapon.pace = weapons["weapons"][key]["pace"]
                    weapon.reliability = weapons["weapons"][key]["reliability"]
                    weapon.save()
                except:
                    pass

            
            character.save()

            return HttpResponse("success", content_type='text/html')
        else:
            return HttpResponse("error", content_type='text/html')
    else:
        return HttpResponse("This is not post zapros fix this", content_type='text/html')

def ajax_save_avatar(request, id):
    if request.method == 'POST':
        character = Character.objects.get(id = id)
        
        try:
            character.avatar_character = request.FILES.get('img', None)
        except MultiValueDictKeyError:
            pass
        character.save()
        return HttpResponse("success")
    else:
        return HttpResponse("error")
    