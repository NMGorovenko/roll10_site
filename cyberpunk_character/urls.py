from django.urls import path, include, re_path
from . import views, ajax
from django.conf import settings


urlpatterns = [
    path('/player_profile/<int:id>', views.CharacterViews, name='new_character'),
    path('/player_profile', views.Profile_playerViews, name="player_profile"),
    path('/player_profile/update_character/<int:id>', ajax.ajax_save_character, name="update_character"),
    path('/player_profile/new_character', views.CreateNewCharacter, name="create_character"),
    path('/player_profile/ajax_return_character/<int:id>', views.ajax_return_character, name="ajax_return_character"),
    path('/player_profile/ajax_return_cybernetic/<int:id>', views.ajax_return_cybernetic, name="ajax_return_cybernetic"),
    path('/player_profile/ajax_return_life_path/<int:id>', views.ajax_return_life_path, name="ajax_return_cybernetic"),
    path('/player_profile/ajax_return_gears/<int:id>', views.ajax_return_gears, name="ajax_return_gears"),
    path('/player_profile/ajax_return_weapons/<int:id>', views.ajax_return_weapons, name="ajax_return_weapons"),

    path('/player_profile/ajax_add_cybernetic/<int:id>', views.ajax_add_cybernetic, name="ajax_add_cybernetic"),
    path('/player_profile/ajax_add_life_events/<int:id>', views.ajax_add_life_events, name="ajax_add_life_events"),
    path('/player_profile/ajax_add_gears/<int:id>', views.ajax_add_gears, name="ajax_add_gears"),
    path('/player_profile/ajax_add_weapons/<int:id>', views.ajax_add_weapons, name="ajax_add_weapons"),

    path('/player_profile/update_character/<int:id>', ajax.ajax_save_character, name="update_character"),
    path('/player_profile/get_character', ajax.get_character, name="get_character"),
    path('/player_profile/save_avatar/<int:id>', ajax.ajax_save_avatar, name="save_avatar"),
] 
