from django.apps import AppConfig


class CyberpunkCharacterConfig(AppConfig):
    name = 'cyberpunk_character'
