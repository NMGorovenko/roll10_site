from django.db import models
from django.contrib.auth.models import User
import os

def path_and_rename(instance, filename):
    upload_to = 'avatars'
    ext = filename.split('.')[-1]

    # get filename

    try:
        exists = open(f"media\{upload_to}\{instance.id}.{ext}", 'r')
        if exists is not None:
            exists.close()
            os.remove(f"media\{upload_to}\{instance.id}.{ext}")
    except FileNotFoundError:
        pass


    

    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format("avatar", instance.id)
    # return the whole path to the file


    return os.path.join(upload_to, filename)

class Character(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField("Кличка", max_length=40, default="Новый персонаж")
    reputation = models.PositiveIntegerField("Репутация", default=0)
    score_upgrade = models.PositiveIntegerField("Очки улучшения", default=0)
    Humanity = models.PositiveIntegerField("Человечность", default=0)

    
    # храниение картинки
    avatar_character = models.ImageField("Аватарка", upload_to=path_and_rename, blank=True, null=True)

    SOIO = "SOIO"
    ROCKER = "ROCKER"
    NETRUNNER ="NETRUNNER"
    MEDIA = "MEDIA"
    NOMAD = "NOMAD"
    FIXER = "FIXER"
    COP = "COP"
    CORP = "CORP"
    TECHLE = "TECHLE"
    MEDTECHLE = "MEDTECHLE"
    ROLE = [
        (SOIO,"Соло"),
        (ROCKER,"Рокер"),
        (NETRUNNER,"Нетраннер"),
        (MEDIA,"Журналист"),
        (NOMAD,"Кочевник"),
        (FIXER,"Фиксер"),
        (COP,"Коп"),
        (CORP,"Человек корпорации"),
        (TECHLE,"Техник"),
        (MEDTECHLE,"Медтехник"),
    ]
    role = models.CharField(
        max_length = 10,
        choices = ROLE,
        default=SOIO)


    # основные характеристики
    intellect = models.PositiveIntegerField("Интелект", default=0)
    reflex = models.PositiveIntegerField("Рефлексы", default=0)
    tech = models.PositiveIntegerField("Тех. способности", default=0)
    cool = models.PositiveIntegerField("Хладнокровие", default=0)
    attr = models.PositiveIntegerField("Привлекательность", default=0)
    luck = models.PositiveIntegerField("Удача", default=0)
    ma = models.PositiveIntegerField("Скорость", default=0)
    body = models.PositiveIntegerField("Тип тела", default=0)
    emp = models.PositiveIntegerField("Эмпатия", default=0)
    run = models.PositiveIntegerField("Бег", default=0)
    leap = models.PositiveIntegerField("Прыжок", default=0)
    lift = models.PositiveIntegerField("Подъем", default=0)



    #special abilities
    authority = models.PositiveIntegerField("Авторитет", default=0)
    charismatic_leadership = models.PositiveIntegerField("Харизматичное лидерство", default=0)
    battle_sence = models.PositiveIntegerField("Чувство боя", default=0)
    credibllity = models.PositiveIntegerField("Правдоподобность", default=0)
    family = models.PositiveIntegerField("Семья", default=0)
    interface = models.PositiveIntegerField("Интерфейс", default=0)
    emergency_repair = models.PositiveIntegerField("Аварийный ремонт", default=0)
    medicine = models.PositiveIntegerField("Медецина", default=0)
    resources = models.PositiveIntegerField("Ресурсы", default=0)
    pull = models.PositiveIntegerField("Блат", default=0)


    # зависящии от интелекта
    accounting = models.PositiveIntegerField("Бухгалтерия", default=0)
    anthropology = models.PositiveIntegerField("Антропология", default=0)
    awareness_notice = models.PositiveIntegerField("Замечать/узнавать", default=0)
    biology = models.PositiveIntegerField("Биология", default=0)
    botany = models.PositiveIntegerField("Ботаника", default=0)
    chemistry = models.PositiveIntegerField("Химия", default=0)
    composition = models.PositiveIntegerField("Композиция", default=0)
    diagnose_illness = models.PositiveIntegerField("Диагностика болезней", default=0)
    education_and_Gen_Know = models.PositiveIntegerField("Образование и общие знания", default=0)
    expert = models.PositiveIntegerField("Эксперт", default=0)
    games_of_chance = models.PositiveIntegerField("Азартные игры", default=0)
    geology = models.PositiveIntegerField("Геология", default=0)
    hide_evade = models.PositiveIntegerField("Прятаться/избегать", default=0)
    history = models.PositiveIntegerField("История", default=0)

    language_1 = models.PositiveIntegerField("Язык 1", default=0)
    language_1_name = models.CharField("Язык 1", max_length=40, default="Язык 1")

    language_2 = models.PositiveIntegerField("Язык 2", default=0)
    language_2_name = models.CharField("Язык 2", max_length=40, default="Язык 2")

    language_3 = models.PositiveIntegerField("Язык 3", default=0)
    language_3_name = models.CharField("Язык 3", max_length=40, default="Язык 3")

    use_of_libraries = models.PositiveIntegerField("Использование Библиотек", default=0)
    mathematics = models.PositiveIntegerField("Математика", default=0)
    physics = models.PositiveIntegerField("Физика", default=0)
    programming = models.PositiveIntegerField("Программирование", default=0)
    tracking = models.PositiveIntegerField("Тайная слежка", default=0)
    gambling = models.PositiveIntegerField("Игра на Бирже", default=0)
    network_knowledge = models.PositiveIntegerField("Знание Сети", default=0)
    teaching = models.PositiveIntegerField("Преподавание", default=0)
    survival = models.PositiveIntegerField("Выживание", default=0)
    zoology = models.PositiveIntegerField("Зоология", default=0)


    #attr
    personal_grooming = models.PositiveIntegerField("Внешний вид", default=0)
    wardrobe_and_style = models.PositiveIntegerField("Гардероб и стиль", default=0)


    #body
    endurance = models.PositiveIntegerField("Выносливость", default=0)
    demo_strengh = models.PositiveIntegerField("Проявление силы", default=0)
    swimming = models.PositiveIntegerField("Плавание", default=0)


    #cool/wild
    interrogation = models.PositiveIntegerField("Допрос", default=0)
    intimidate = models.PositiveIntegerField("Угрожать/запугивать", default=0)
    oratory = models.PositiveIntegerField("Красноречие", default=0)
    resist_torture_drugs = models.PositiveIntegerField("Устойчивость к наркоте/пыткам", default=0)
    streetwise = models.PositiveIntegerField("Знание улиц", default=0)

    #empathy
    human_understanding = models.PositiveIntegerField("Понимание людей", default=0)
    interview = models.PositiveIntegerField("Интервью", default=0)
    leadership = models.PositiveIntegerField("Лидерство", default=0)
    seduction = models.PositiveIntegerField("Соблазнение", default=0)
    good_manners = models.PositiveIntegerField("Хорошие манеры", default=0)
    conviction = models.PositiveIntegerField("Убеждать/заговар. зубы", default=0)
    public_performance = models.PositiveIntegerField("Публичные выступления", default=0)


    #ref
    bow_crossbow = models.PositiveIntegerField("Лук/Арбалет", default=0)
    athletics = models.PositiveIntegerField("Атлетика", default=0)
    fight = models.PositiveIntegerField("Борьба", default=0)
    dance = models.PositiveIntegerField("Танцевать", default=0)
    evasion = models.PositiveIntegerField("Уклонение", default=0)
    driving = models.PositiveIntegerField("Вождение", default=0)
    fencing = models.PositiveIntegerField("Фехтование", default=0)
    pistol = models.PositiveIntegerField("Пистолет", default=0)
    heavy_weapon = models.PositiveIntegerField("Тяжелое оружие", default=0)
    
    
    martial_arts_1 = models.PositiveIntegerField("БИ 1", default=0)
    martial_arts_1_name = models.CharField("БИ 1", max_length=40, default="БИ 1")

    martial_arts_2 = models.PositiveIntegerField("БИ 2", default=0)
    martial_arts_2_name = models.CharField("БИ 2", max_length=40, default="БИ 2")

    martial_arts_3 = models.PositiveIntegerField("БИ 3", default=0)
    martial_arts_3_name = models.CharField("БИ 3", max_length=40, default="БИ 3")

    melee = models.PositiveIntegerField("Ближний бой", default=0)
    motorcycle_control = models.PositiveIntegerField("Управление мотоциклом", default=0)
    transport_management = models.PositiveIntegerField("Управление тяж. транспортом", default=0)
    helicopter_piloting = models.PositiveIntegerField("Пилотирование(вертолет)", default=0)
    airplane_piloting = models.PositiveIntegerField("Пилотирование(Самолет)", default=0)
    airship_piloting = models.PositiveIntegerField("Пилотирование(Дережабль)", default=0)
    av_aerodine_piloting = models.PositiveIntegerField("Пилот. (AV/Аэродайн)", default=0)
    gun = models.PositiveIntegerField("Ружье", default=0)
    stealth = models.PositiveIntegerField("Скрытность", default=0)
    submachine_gun = models.PositiveIntegerField("Пистолет-пулемет", default=0)


    #tech
    technology_airplane = models.PositiveIntegerField("Технология(самолет)", default=0)
    technology_av_aerodine = models.PositiveIntegerField("Технология(AV/Аэродайн)", default=0)
    basic_technology = models.PositiveIntegerField("Базовая технология", default=0)
    cryocamera_control = models.PositiveIntegerField("Управление криокамерой", default=0)
    technology_cyberdeck = models.PositiveIntegerField("Технология(кибердека)", default=0)
    cyber_technology = models.PositiveIntegerField("Кибертехнология", default=0)
    explosives = models.PositiveIntegerField("Взрывчатка", default=0)
    disguise = models.PositiveIntegerField("Маскировка", default=0)
    electronics = models.PositiveIntegerField("Электроника", default=0)
    electronic_security = models.PositiveIntegerField("Электронная безопасность", default=0)
    first_aid = models.PositiveIntegerField("Первая помощь", default=0)
    fake = models.PositiveIntegerField("Подделка", default=0)
    technology_helicopter = models.PositiveIntegerField("Технология(Вертолет)", default=0)
    painting = models.PositiveIntegerField("Рисование", default=0)
    cinema_photography = models.PositiveIntegerField("Кино-фотосъемка", default=0)
    pharmaceuticals = models.PositiveIntegerField("Фармацевтика", default=0)
    opening_locks = models.PositiveIntegerField("Вскрытие замков", default=0)
    steal = models.PositiveIntegerField("Кража", default=0)
    playing_instrument = models.PositiveIntegerField("Игра на инструменте", default=0)
    technology_weapon = models.PositiveIntegerField("Технология(Оружие)", default=0)



    # одежда
    clothing = models.CharField("одежда", max_length=100, default="", blank=True, null=True)
    hair = models.CharField("прическа", max_length=100, default="", blank=True, null=True)
    affectations = models.CharField("назначение", max_length=100, default="", blank=True, null=True)
    ethnlcity = models.CharField("этнос", max_length=100, default="", blank=True, null=True)
    language = models.CharField("язык", max_length=100, default="", blank=True, null=True)

    # Семья
    family_text = models.TextField("Семья", max_length=100, default="" , blank=True, null=True)
    brothers = models.PositiveIntegerField("Братьев", default=0, blank=True, null=True)
    sisters = models.PositiveIntegerField("Сестер", default=0, blank=True, null=True)

    # Мотивация
    motivation = models.CharField("мотивация", max_length=100, default="", blank=True, null=True)
    trait = models.CharField("черта характера", max_length=100, default="", blank=True, null=True)
    value_most = models.CharField("ценишь больше всего", max_length=100, default="", blank=True, null=True)
    attitude_to_people = models.CharField("отношение к людям", max_length=100, default="", blank=True, null=True)
    significant_subject = models.CharField("значимый предмет", max_length=100, default="", blank=True, null=True)

    def __str__(self):
        return self.name


class Cybernetics(models.Model):
    character_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    type_cyber = models.CharField("Тип кибернетики", max_length=40, default="", blank=True, null=True)
    hl = models.PositiveIntegerField("hl", default=0, blank=True, null=True)
    cost = models.PositiveIntegerField("cost", default=0, blank=True, null=True)
    
    def __str__(self):
        return self.type_cyber


class Life_events(models.Model):
    character_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    event = models.CharField("событие жизни", max_length=40, default="", blank=True, null=True)
    year = models.PositiveIntegerField("год", default=0, null=True, blank=True)

    def __str__(self):
        return str(self.year)


class Gear(models.Model):
    character_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    type_gear = models.CharField("Тип снаряжения", default="", max_length=40, blank=True, null=True)
    hl = models.PositiveIntegerField("hl", default=0, blank=True, null=True)
    cost = models.PositiveIntegerField("cost", default=0, blank=True, null=True)

    def __str__(self):
        return self.type_gear


class Weapon(models.Model):
    character_id = models.ForeignKey(Character, on_delete=models.CASCADE)

    name = models.CharField("Название", max_length=40, default="", blank=True, null=True)
    type_weapon = models.CharField("Тип", max_length=40, default="", blank=True, null=True)
    
    accuracy = models.PositiveIntegerField("Точность", default=0, blank=True, null=True)
    conc = models.PositiveIntegerField("Скрыт", default=0, blank=True, null=True)
    avall = models.PositiveIntegerField("Дост", default=0, blank=True, null=True)
    dam = models.PositiveIntegerField("ПОВР", default=0, blank=True, null=True)
    shells = models.PositiveIntegerField("Снаряды", default=0, blank=True, null=True)
    pace = models.PositiveIntegerField("ТЕМП СТРЕЛ", default=0, blank=True, null=True)
    reliability = models.PositiveIntegerField("Надежность", default=0, blank=True, null=True)


    def __str__(self):
        return self.name