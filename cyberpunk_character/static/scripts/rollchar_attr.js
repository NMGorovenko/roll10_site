$(document).ready(function(){
    $("#roll_attr").on('click', function(){
        var val_attr = Number($('#attr').val());
        var personal_grooming_val = Number($('#personal_grooming').val());
        var wardrobe_and_style_val = Number($('#wardrobe_and_style').val());

        var attr_abil = {
            "Внешний вид": personal_grooming_val,
            "Гардероб и стиль": wardrobe_and_style_val,
        };

        Swal.fire({
            html: `<div id='modal_abil_attr'>
                  <div>
                  <h3>Прокидывание</h3>
                  <h4>Привлекательность</h4>
                  <br><br>
                  <p>Выберите способность:</p>
                  <select id='abil_attr'>
                  <option name='abillity_attr'>Внешний вид</option>
                  <option name='abillity_attr'>Гардероб и стиль</option>
                  </select>
                  </div>
                  <br><br>
                  <button id='roll_abil_attr'>Кинуть кости</button>
                  </div>`,
                  showConfirmButton: false,
        });
        $('#roll_abil_attr').on('click', function(){

            var selected_abil = $('#abil_attr').val();
            for (let k in attr_abil){

                if (k == selected_abil){
                    var cur_abil = attr_abil[k];
                    break;
                }
            }

            let dice = Math.floor((Math.random() * 11) % 10 + 1);

            $('#rolled_attr').remove();
            let roll = dice + val_attr + cur_abil;
            $('#modal_abil_attr').append(`<p id='rolled_attr'>Привлекательность (${val_attr}) +
              ${selected_abil} (${cur_abil}) + Бросок костей (${dice}) = ${roll}</p>`);
        })
    })
})
