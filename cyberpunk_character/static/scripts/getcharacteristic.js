//ajax_return_character/ - общий путь JSON'ов
//window.location.href

// Получение id выбранного персонажа
function GetCharUrl(){
    let cur_url = window.location.href;
    var number_of_border;

    // Определение границы '/' сслыки после которой идет id персонажа
    for (let i = cur_url.length - 1; cur_url[i] != '/'; i--){
        number_of_border = i;
    }

    var number_of_char = cur_url[number_of_border];

    // Определение номера персонажа
    for (let i = number_of_border + 1; i < cur_url.length; i++){
        number_of_char += cur_url[i];
    }

    return number_of_char;
};

var number_of_curchar = GetCharUrl();


$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: `ajax_return_character/${number_of_curchar}`,
        dataType: "json",
        data: {text: "text"},
        success: function(data){
            for (let key in data){
               $(`input[name="${key}"]`).val(data[key]);
            }
            // Роль
            $(`option[id="${data.select_role}"]`).attr('selected', 'selected');
            //Семья тект
            $("#family_text").val(data.family_text);
            //аватар
            $('a#avatar').append(`<img src="${data.avatar_character}" 
                                 style="width:500px; height:500px; 
                                 border-radius: 10px;" alt="Загрузить картинку">`);
            $('form#upd_char').attr('action', `update_character/${number_of_curchar}`);
            //Подъем
            $('input[name="lift"]').val(40 * Number($('input[name="body"]').val()));
            //Бег
            $('input[name="run"]').val(3 * Number($('input[name="ma"]').val()));
            //Прыжок
            $('input[name="leap"]').val(Math.round(3/4 * Number($('input[name="ma"]').val())));
            console.log("характеристики, аватар отображены");
        }
    })
    // В случае изменения скорости
    $('input[name="ma"]').on('change', function(){
        $('input[name="run"]').val(3 * Number($('input[name="ma"]').val()));
        $('input[name="leap"]').val(Math.round(3/4 * Number($('input[name="ma"]').val())));
    })
    // В случае изменения типа тела
    $('input[name="body"]').on('change', function(){
        $('input[name="lift"]').val(40 * Number($('input[name="body"]').val()));
    })
})
