$(document).ready(function(){
    $("#roll_reflex").on('click', function(){
        var val_reflex = Number($('#reflex').val());
        var bow_crossbow_val = Number($('#bow_crossbow').val());
        var athletics_val = Number($('#athletics').val());
        var fight_val = Number($('#fight').val());
        var dance_val = Number($('#dance').val());
        var evasion_val = Number($('#evasion').val());
        var driving_val = Number($('#driving').val());
        var fencing_val = Number($('#fencing').val());
        var pistol_val = Number($('#pistol').val());
        var heavy_weapon_val = Number($('#heavy_weapon').val());
        var martial_arts_1_val = Number($('#martial_arts_1').val());
        var martial_arts_2_val = Number($('#martial_arts_2').val());
        var martial_arts_3_val = Number($('#martial_arts_3').val());
        var melee_val = Number($('#melee').val());
        var motorcycle_control_val = Number($('#motorcycle_control').val());
        var transport_management_val = Number($('#transport_management').val());
        var helicopter_piloting_val = Number($('#helicopter_piloting').val());
        var airplane_piloting_val = Number($('#airplane_piloting').val());
        var airship_piloting_val = Number($('#airship_piloting').val());
        var av_aerodine_piloting_val = Number($('#av_aerodine_piloting').val());
        var gun_val = Number($('#gun').val());
        var stealth_val = Number($('#stealth').val());
        var submachine_gun_val = Number($('#submachine_gun').val());

        var reflex_abil = {
            "Лук/Арбалет": bow_crossbow_val,
            "Атлетика": athletics_val,
            "Борьба": fight_val,
            "Танцевать": dance_val,
            "Уклонение": evasion_val,
            "Вождение": driving_val,
            "Фехтование": fencing_val,
            "Пистолет": pistol_val,
            "Тяжелое оружие": heavy_weapon_val,
            "Боевое искусство 1": martial_arts_1_val,
            "Боевое искусство 2": martial_arts_2_val,
            "Боевое искусство 3": martial_arts_3_val,
            "Ближний бой": melee_val,
            "Управление мотоциклом": motorcycle_control_val,
            "Управление тяж. транспортом": transport_management_val,
            "Пилотирование(вертолет)": helicopter_piloting_val,
            "Пилотирование(Самолет)": airplane_piloting_val,
            "Пилотирование(Дережабль)": airship_piloting_val,
            "Пилот. (AV/Аэродайн)": av_aerodine_piloting_val,
            "Ружье": gun_val,
            "Скрытность":  stealth_val,
            "Пистолет-пулемет": submachine_gun_val,
        };

        Swal.fire({
            html: `<div id='modal_abil_reflex'>
                  <div>
                  <h3>Прокидывание</h3>
                  <h4>Рефлексы</h4>
                  <br><br>
                  <p>Выберите способность:</p>
                  <select id='abil_reflex'>
                  <option name='abillity_reflex'>Лук/Арбалет</option>
                  <option name='abillity_reflex'>Атлетика</option>
                  <option name='abillity_reflex'>Борьба</option>
                  <option name='abillity_reflex'>Танцевать</option>
                  <option name='abillity_reflex'>Уклонение</option>
                  <option name='abillity_reflex'>Вождение</option>
                  <option name='abillity_reflex'>Фехтование</option>
                  <option name='abillity_reflex'>Пистолет</option>
                  <option name='abillity_reflex'>Тяжелое оружие</option>
                  <option name='abillity_reflex'>Боевое искусство 1</option>
                  <option name='abillity_reflex'>Боевое искусство 2</option>
                  <option name='abillity_reflex'>Боевое искусство 3</option>
                  <option name='abillity_reflex'>Ближний бой</option>
                  <option name='abillity_reflex'>Управление мотоциклом</option>
                  <option name='abillity_reflex'>Управление тяж. транспортом</option>
                  <option name='abillity_reflex'>Пилотирование(вертолет)</option>
                  <option name='abillity_reflex'>Пилотирование(Самолет)</option>
                  <option name='abillity_reflex'>Пилотирование(Дережабль)</option>
                  <option name='abillity_reflex'>Пилот. (AV/Аэродайн)</option>
                  <option name='abillity_reflex'>Ружье</option>
                  <option name='abillity_reflex'>Скрытность</option>
                  <option name='abillity_reflex'>Пистолет-пулемет</option>
                  </select>
                  </div>
                  <br><br>
                  <button id='roll_abil_reflex'>Кинуть кости</button>
                  </div>`,
                  showConfirmButton: false,
        });
        $('#roll_abil_reflex').on('click', function(){

            var selected_abil = $('#abil_reflex').val();
            for (let k in reflex_abil){

                if (k == selected_abil){
                    var cur_abil = reflex_abil[k];
                    break;
                }
            }

            let dice = Math.floor((Math.random() * 11) % 10 + 1);

            $('#rolled_reflex').remove();
            let roll = dice + val_reflex + cur_abil;
            $('#modal_abil_reflex').append(`<p id='rolled_reflex'>Рефлекс (${val_reflex}) +
              ${selected_abil} (${cur_abil}) + Бросок костей (${dice}) = ${roll}</p>`);
        })
    })
})
