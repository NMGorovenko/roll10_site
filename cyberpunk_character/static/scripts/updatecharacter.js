//update_character/id
function GetCharUrl(){
    let cur_url = window.location.href;
    var number_of_border;

    // Определение границы '/' сслыки после которой идет id персонажа
    for (let i = cur_url.length - 1; cur_url[i] != '/'; i--){
        number_of_border = i;
    }

    var number_of_char = cur_url[number_of_border];

    // Определение номера персонажа
    for (let i = number_of_border + 1; i < cur_url.length; i++){
        number_of_char += cur_url[i];
    }

    return number_of_char;
};

var number_of_curchar = GetCharUrl();


$(document).ready(function(){
    var csrftoken = $('input[name="csrfmiddlewaretoken"]').attr('value');
    var role_select = {
        "Соло": 'SOIO',
        "Рокер": 'ROCKER',
        "Нетраннер": 'NETRUNNER',
        "Журналист": 'MEDIA',
        "Кочевник": 'NOMAD',
        "Фиксер": 'FIXER',
        "Коп": 'COP',
        "Человек корпорации": 'CORP',
        "Техник": 'TECHLE',
        "Медтехник": 'MEDTECHLE',
    }

    $("#update_char").on('click', function(){

        var selected_role = $("select[name='select_role']").val();


        //Собираем таблицу снаряжения для отправки:
        data_gears = {};
        data_gears["gears"] = {};
        gears = $("#gear").find('input');
        for (let j of gears){
            var name_g = j.name;
            var cur_key = "";
            for (let k = name_g.length - 1; k > 0; k --){
                if (!isNaN(Number(name_g[k]))){
                    cur_key = name_g[k] + cur_key;
                }
            }
            data_gears["gears"][cur_key] = {};
            cur_key = "";
        }
        for (let j of gears){
            var name_g = j.name;
            var cur_key = "";
            for (let k = name_g.length - 1; k > 0; k --){
                if (!isNaN(Number(name_g[k]))){
                    cur_key = name_g[k] + cur_key;
                    name_g = name_g.substring(0, k);
                }
            }
            data_gears["gears"][cur_key][name_g] = j.value;
            cur_key = "";
        }// Конеу сборки таблицы снаряги


        //Собираем таблицу оружия для отправки:
        data_weapons = {};
        data_weapons["weapons"] = {};
        weapons = $("#weapon").find('input');
        for (let j of weapons){
            var name_w = j.name;
            var cur_key = "";
            for (let k = name_w.length - 1; k > 0; k --){
                if (!isNaN(Number(name_w[k]))){
                    cur_key = name_w[k] + cur_key;
                }
            }
            data_weapons["weapons"][cur_key] = {};
            cur_key = "";
        }
        for (let j of weapons){
            var name_w = j.name;
            var cur_key = "";
            for (let k = name_w.length - 1; k > 0; k --){
                if (!isNaN(Number(name_w[k]))){
                    cur_key = name_w[k] + cur_key;
                    name_w = name_w.substring(0, k); 
                }
            }
            data_weapons["weapons"][cur_key][name_w] = j.value;
            cur_key = "";
        }// Конеу сборки таблицы оружия


        //Собираем таблицу событий жизни для отправки:
        data_lifeev = {};
        data_lifeev["life_events"] = {};
        life_path = $("#life_path").find('input');
        for (let j of life_path){
            var name_lp = j.name;
            var cur_key = "";
            for (let k = name_lp.length - 1; k > 0; k --){
                if (!isNaN(Number(name_lp[k]))){
                    cur_key = name_lp[k] + cur_key;
                }
            }
            data_lifeev["life_events"][cur_key] = {};
            cur_key = "";
        }
        for (let j of life_path){
            var name_lp = j.name
            var cur_key = "";
            for (let k = name_lp.length - 1; k > 0; k --){
                if (!isNaN(Number(name_lp[k]))){
                    cur_key = name_lp[k] + cur_key;
                    name_lp = name_lp.substring(0, k); 
                }
            }
            data_lifeev["life_events"][cur_key][name_lp] = j.value;
            cur_key = "";
        }// Конеу сборки таблицы событий жизни


        //Собираем таблицу cybernetic для отправки:
        data_cyber = {}
        data_cyber["cybernetics"] = {};
        cybernetic = $("#cybernetic").find('input');
        for (let j of cybernetic){
            var name_c = j.name;
            var cur_key = "";
            for (let k = name_c.length - 1; k > 0; k --){
                if (!isNaN(Number(name_c[k]))){
                    cur_key = name_c[k] + cur_key;
                }
            }
            data_cyber["cybernetics"][cur_key] = {};
            cur_key = "";
        }
        for (let j of cybernetic){
            var name_c = j.name
            var cur_key = "";
            for (let k = name_c.length - 1; k > 0; k --){
                if (!isNaN(Number(name_c[k]))){
                    cur_key = name_c[k] + cur_key;
                    name_c = name_c.substring(0, k); 
                }
            }
            data_cyber["cybernetics"][cur_key][name_c] = j.value;
            cur_key = "";
        }// Конеу сборки таблицы cybernetics


        var data_send = {"nickname": $("input[name='nickname']").val(),
        "reputation":$("input[name='reputation']").val(),
        "Humanity": $("input[name='Humanity']").val(),
        "score_upgrade": $("input[name='score_upgrade']").val(),
        "select_role": role_select[selected_role],
        "intellect": $("input[name='intellect']").val(),
        "reflex": $("input[name='reflex']").val(),
        "tech": $("input[name='tech']").val(),
        "cool": $("input[name='cool']").val(),
        "attr": $("input[name='attr']").val(), 
        "luck": $("input[name='luck']").val(), 
        "ma": $("input[name='ma']").val(),
        "body": $("input[name='body']").val(),
        "emp": $("input[name='emp']").val(), 
        "run": $("input[name='run']").val(), 
        "leap": $("input[name='leap']").val(), 
        "lift": $("input[name='lift']").val(), 
        "authority": $("input[name='authority']").val(), 
        "charismatic_leadership": $("input[name='charismatic_leadership']").val(), 
        "battle_sence": $("input[name='battle_sence']").val(), 
        "credibllity": $("input[name='credibllity']").val(), 
        "family": $("input[name='family']").val(), 
        "interface": $("input[name='interface']").val(), 
        "emergency_repair": $("input[name='emergency_repair']").val(), 
        "medicine": $("input[name='medicine']").val(), 
        "resources": $("input[name='resources']").val(), 
        "pull": $("input[name='pull']").val(), 
        "accounting": $("input[name='accounting']").val(), 
        "anthropology": $("input[name='anthropology']").val(), 
        "awareness_notice": $("input[name='awareness_notice']").val(), 
        "biology": $("input[name='biology']").val(), 
        "botany": $("input[name='botany']").val(),
        "chemistry": $("input[name='chemistry']").val(),
        "composition": $("input[name='composition']").val(), 
        "diagnose_illness": $("input[name='diagnose_illness']").val(),
        "education_and_Gen_Know": $("input[name='education_and_Gen_Know']").val(),
        "expert": $("input[name='expert']").val(),
        "games_of_chance": $("input[name='games_of_chance']").val(),
        "geology": $("input[name='geology']").val(),
        "hide_evade": $("input[name='hide_evade']").val(),
        "history": $("input[name='history']").val(),
        "language_1": $("input[name='language_1']").val(),
        "language_1_name": $("input[name='language_1_name']").val(),
        "language_2": $("input[name='language_2']").val(),
        "language_2_name": $("input[name='language_2_name']").val(),
        "language_3": $("input[name='language_3']").val(),
        "language_3_name": $("input[name='language_3_name']").val(),
        "use_of_libraries": $("input[name='use_of_libraries']").val(),
        "mathematics": $("input[name='mathematics']").val(),
        "physics": $("input[name='physics']").val(),
        "programming": $("input[name='programming']").val(),
        "tracking": $("input[name='tracking']").val(),
        "gambling": $("input[name='gambling']").val(),
        "network_knowledge": $("input[name='network_knowledge']").val(),
        "teaching": $("input[name='teaching']").val(),
        "survival": $("input[name='survival']").val(),
        "zoology": $("input[name='zoology']").val(),
        "personal_grooming": $("input[name='personal_grooming']").val(),
        "wardrobe_and_style": $("input[name='wardrobe_and_style']").val(),
        "endurance": $("input[name='endurance']").val(), 
        "demo_strengh": $("input[name='demo_strengh']").val(),
        "swimming": $("input[name='swimming']").val(),
        "interrogation": $("input[name='interrogation']").val(),
        "intimidate": $("input[name='intimidate']").val(),
        "oratory": $("input[name='oratory']").val(),
        "resist_torture_drugs": $("input[name='resist_torture_drugs']").val(),
        "streetwise": $("input[name='streetwise']").val(),
        "human_understanding": $("input[name='human_understanding']").val(),
        "interview": $("input[name='interview']").val(),
        "leadership": $("input[name='leadership']").val(),
        "seduction": $("input[name='seduction']").val(),
        "good_manners": $("input[name='good_manners']").val(),
        "conviction": $("input[name='conviction']").val(),
        "public_performance": $("input[name='public_performance']").val(),
        "bow_crossbow": $("input[name='bow_crossbow']").val(),
        "athletics": $("input[name='athletics']").val(),
        "fight": $("input[name='fight']").val(),
        "dance": $("input[name='dance']").val(),
        "evasion": $("input[name='evasion']").val(),
        "driving": $("input[name='driving']").val(),
        "fencing": $("input[name='fencing']").val(),
        "pistol": $("input[name='pistol']").val(),
        "heavy_weapon": $("input[name='heavy_weapon']").val(),
        "martial_arts_1": $("input[name='martial_arts_1']").val(),
        "martial_arts_1_name": $("input[name='martial_arts_1_name']").val(),
        "martial_arts_2": $("input[name='martial_arts_2']").val(),
        "martial_arts_2_name": $("input[name='martial_arts_2_name']").val(),
        "martial_arts_3": $("input[name='martial_arts_3']").val(),
        "martial_arts_3_name": $("input[name='martial_arts_3_name']").val(),
        "melee": $("input[name='melee']").val(),
        "motorcycle_control": $("input[name='motorcycle_control']").val(),
        "transport_management": $("input[name='transport_management']").val(),
        "helicopter_piloting": $("input[name='helicopter_piloting']").val(),
        "airplane_piloting": $("input[name='airplane_piloting']").val(),
        "airship_piloting": $("input[name='airship_piloting']").val(),
        "av_aerodine_piloting": $("input[name='av_aerodine_piloting']").val(),
        "gun": $("input[name='gun']").val(),
        "stealth": $("input[name='stealth']").val(),
        "submachine_gun": $("input[name='submachine_gun']").val(),
        "technology_airplane": $("input[name='technology_airplane']").val(),
        "technology_av_aerodine": $("input[name='technology_av_aerodine']").val(),
        "basic_technology": $("input[name='basic_technology']").val(),
        "cryocamera_control": $("input[name='cryocamera_control']").val(),
        "technology_cyberdeck": $("input[name='technology_cyberdeck']").val(),
        "cyber_technology": $("input[name='cyber_technology']").val(),
        "explosives": $("input[name='explosives']").val(),
        "disguise": $("input[name='disguise']").val(),
        "electronics": $("input[name='electronics']").val(),
        "electronic_security": $("input[name='electronic_security']").val(),
        "first_aid": $("input[name='first_aid']").val(),
        "fake": $("input[name='fake']").val(),
        "technology_helicopter": $("input[name='technology_helicopter']").val(),
        "painting": $("input[name='painting']").val(),
        "cinema_photography": $("input[name='cinema_photography']").val(),
        "pharmaceuticals": $("input[name='pharmaceuticals']").val(),
        "opening_locks": $("input[name='opening_locks']").val(),
        "steal": $("input[name='steal']").val(),
        "playing_instrument": $("input[name='playing_instrument']").val(),
        "technology_weapon": $("input[name='technology_weapon']").val(),
        "brothers": $("input[name='brothers']").val(),
        "sisters": $("input[name='sisters']").val(),
        "clothes": $("input[name='clothes']").val(),
        "hairstyle": $("input[name='hairstyle']").val(),
        "chip": $("input[name='chip']").val(),
        "race": $("input[name='race']").val(),
        "language": $("input[name='language']").val(),
        "family_text": $("#family_text").val(),
        "motivation": $("input[name='motivation']").val(),
        "trait": $("input[name='trait']").val(),
        "value_most": $("input[name='value_most']").val(),
        "attitude_to_people": $("input[name='attitude_to_people']").val(),
        "significant_subject": $("input[name='significant_subject']").val(),
        "gears": JSON.stringify(data_gears),
        "cyber": JSON.stringify(data_cyber),
        "life_events": JSON.stringify(data_lifeev),
        "weapons": JSON.stringify(data_weapons),
    
        "csrfmiddlewaretoken": csrftoken,
        };


        console.log(data_send);
        $.ajax({
            type: "POST",
            dataType: "text",
            url: `update_character/${number_of_curchar}`,
            data: data_send,
            cache:false,
            success: function(data){
                if (data == "success"){
                    console.log("Данные персонажа успешно изменены");
                    Swal.fire({
                        title: "Данные персонажа были успешно изменены!",
                        icon: "success",
                        showConfirmButton: false,
                    });
                }
                else if(data != "error"){
                    console.log("Данные неуспешно отправлены)");
                    Swal.fire({
                        title: "Данные персонажа не были изменены!",
                        text: "Ошибка приема сервером",
                        icon: "error",
                        showConfirmButton: false,
                    });
                }
            },
            error: function(data){
                console.log("Ошибка отправки");
                Swal.fire({
                    title: "Данные персонажа не были изменены!",
                    text: "Ошибка отправки данных",
                    icon: "error",
                    showConfirmButton: false,
                });
            }
        })
    })
})