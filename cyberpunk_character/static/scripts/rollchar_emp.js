$(document).ready(function(){
    $("#roll_emp").on('click', function(){
        var val_emp = Number($('#emp').val());
        var human_understanding_val = Number($('#human_understanding').val());
        var interview_val = Number($('#interview').val());
        var leadership_val = Number($('#leadership').val());
        var seduction_val = Number($('#seduction').val());
        var good_manners_val = Number($('#good_manners').val());
        var conviction_val = Number($('#conviction').val());
        var public_performance_val = Number($('#public_performance').val());

        var emp_abil = {
            "Понимание людей": human_understanding_val,
            "Интервью": interview_val,
            "Лидерство": leadership_val,
            "Соблазнение": seduction_val,
            "Хорошие манеры": good_manners_val,
            "Убеждать/заговар. зубы": conviction_val,
            "Публичные выступления": public_performance_val,
        };

        Swal.fire({
            html: `<div id='modal_abil_emp'>
                  <div>
                  <h3>Прокидывание</h3>
                  <h4>Эмпатия</h4>
                  <br><br>
                  <p>Выберите способность:</p>
                  <select id='abil_emp'>
                  <option name='abillity_emp'>Понимание людей</option>
                  <option name='abillity_emp'>Интервью</option>
                  <option name='abillity_emp'>Лидерство</option>
                  <option name='abillity_emp'>Соблазнение</option>
                  <option name='abillity_emp'>Хорошие манеры</option>
                  <option name='abillity_emp'>Убеждать/заговар. зубы</option>
                  <option name='abillity_emp'>Публичные выступления</option>
                  </select>
                  </div>
                  <br><br>
                  <button id='roll_abil_emp'>Кинуть кости</button>
                  </div>`,
                  showConfirmButton: false,
        });
        $('#roll_abil_emp').on('click', function(){

            var selected_abil = $('#abil_emp').val();
            for (let k in emp_abil){

                if (k == selected_abil){
                    var cur_abil = emp_abil[k];
                    break;
                }
            }

            let dice = Math.floor((Math.random() * 11) % 10 + 1);

            $('#rolled_emp').remove();
            let roll = dice + val_emp + cur_abil;
            $('#modal_abil_emp').append(`<p id='rolled_emp'>Эмпатия (${val_emp}) +
              ${selected_abil} (${cur_abil}) + Бросок костей (${dice}) = ${roll}</p>`);
        })
    })
})
