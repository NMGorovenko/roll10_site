$(document).ready(function(){
    $("#roll_int").on('click', function(){
        var val_intellect = Number($('#intellect').val());
        var botany_val = Number($('#botany').val());
        var accounting_val = Number($('#accounting').val());
        var anthropology_val = Number($('#anthropology').val());
        var awareness_Notice_val = Number($('#awareness_Notice').val());
        var biology_val = Number($('#biology').val());
        var chemistry_val = Number($('#chemistry').val());
        var composition_val = Number($('#composition').val());
        var diagnose_illness_val = Number($('#diagnose_illness').val());
        var education_and_Gen_Know_val = Number($('#education_and_Gen_Know').val());
        var expert_val = Number($('#expert').val());
        var games_of_chance_val = Number($('#games_of_chance').val());
        var geology_val = Number($('#geology').val());
        var hide_evade_val = Number($('#hide_evade').val());
        var history_val = Number($('#history').val());
        var language_1_val = Number($('#language_1').val());
        var language_2_val = Number($('#language_2').val());
        var language_3_val = Number($('#language_3').val());
        var use_of_libraries_val = Number($('#use_of_libraries').val());
        var mathematics_val = Number($('#mathematics').val());
        var physics_val = Number($('#physics').val());
        var programming_val = Number($('#programming').val());
        var tracking_val = Number($('#tracking').val());
        var gambling_val = Number($('#gambling').val());
        var network_knowledge_val = Number($('#network_knowledge').val());
        var teaching_val = Number($('#teaching').val());
        var survival_val = Number($('#survival').val());
        var zoology_val = Number($('#zoology').val());

        var intellect_abil = {
            "Бухгалтерия": accounting_val,
            "Антропология": anthropology_val,
            "Замечать/узнавать": awareness_Notice_val,
            "Биология": biology_val,
            "Ботаника": botany_val,
            "Химия": chemistry_val,
            "Композиция": composition_val,
            "Диагностика болезней": diagnose_illness_val,
            "Образование и общие знания": education_and_Gen_Know_val,
            "Эксперт": expert_val,
            "Азартные игры": games_of_chance_val,
            "Геология": geology_val,
            "Прятаться/избегать": hide_evade_val,
            "История": history_val,
            "Язык 1": language_1_val,
            "Язык 2": language_2_val,
            "Язык 3": language_3_val,
            "Использование Библиотек": use_of_libraries_val,
            "Математика": mathematics_val,
            "Физика": physics_val,
            "Программирование": programming_val,
            "Следить/шпионить": tracking_val,
            "Игра на Бирже": gambling_val,
            "Знание Сети": network_knowledge_val,
            "Преподавание": teaching_val,
            "Выживание": survival_val,
            "Зоология": zoology_val,
        };

        Swal.fire({
            html: `<div id='modal_abil_int'>
                  <div>
                  <h3>Прокидывание</h3>
                  <h4>Интеллект</h4>
                  <br><br>
                  <p>Выберите способность:</p>
                  <select id='abil_int'>
                  <option name='abillity_int'>Бухгалтерия</option>
                  <option name='abillity_int'>Антропология</option>
                  <option name='abillity_int'>Замечать/узнавать</option>
                  <option name='abillity_int'>Биология</option>
                  <option name='abillity_int'>Ботаника</option>
                  <option name='abillity_int'>Химия</option>
                  <option name='abillity_int'>Композиция</option>
                  <option name='abillity_int'>Диагностика болезней</option>
                  <option name='abillity_int'>Образование и общие знания</option>
                  <option name='abillity_int'>Эксперт</option>
                  <option name='abillity_int'>Азартные игры</option>
                  <option name='abillity_int'>Геология</option>
                  <option name='abillity_int'>Прятаться/избегать</option>
                  <option name='abillity_int'>История</option>
                  <option name='abillity_int'>Язык 1</option>
                  <option name='abillity_int'>Язык 2</option>
                  <option name='abillity_int'>Язык 3</option>
                  <option name='abillity_int'>Использование Библиотек</option>
                  <option name='abillity_int'>Математика</option>
                  <option name='abillity_int'>Физика</option>
                  <option name='abillity_int'>Программирование</option>
                  <option name='abillity_int'>Следить/шпионить</option>
                  <option name='abillity_int'>Игра на Бирже</option>
                  <option name='abillity_int'>Знание Сети</option>
                  <option name='abillity_int'>Преподавание</option>
                  <option name='abillity_int'>Выживание</option>
                  <option name='abillity_int'>Зоология</option>
                  </select>
                  </div>
                  <br><br>
                  <button id='roll_abil_int'>Кинуть кости</button>
                  </div>`,
                  showConfirmButton: false,
        });
        $('#roll_abil_int').on('click', function(){

            var selected_abil = $('#abil_int').val();
            for (let k in intellect_abil){

                if (k == selected_abil){
                    var cur_abil = intellect_abil[k];
                    break;
                }
            }

            let dice = Math.floor((Math.random() * 11) % 10 + 1);

            $('#rolled_int').remove();
            let roll = dice + val_intellect + cur_abil;
            $('#modal_abil_int').append(`<p id='rolled_int'>Интеллект (${val_intellect})
              + ${selected_abil} (${cur_abil}) + Бросок костей (${dice}) = ${roll}</p>`);
        })
    })
})
