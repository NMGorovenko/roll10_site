$(document).ready(function(){

    $('#open_modal').on('click', function(){
        Swal.fire({
            html: "<div id='modal'>" +
                "<div>" +
                "<p>Сколько костей?</p>" +
                "<input id='numberofdices' class='short_input' type='number' min='1' max='10' value='1'>" +
                "</div>" +
                "<br><br>" +
                "<div>" +
                "<p>Сколько граней у куба?</p>" +
                "<select id='facets'>" +
                "<option name='facet'>2</option>" +
                "<option name='facet' id='dise_four'>4</option>" +
                "<option name='facet' id='dise_six'>6</option>" +
                "<option name='facet' id='dise_eight'>8</option>" +
                "<option name='facet' id='dise_ten'>10</option>" +
                "</select>" +
                "</div>" +
                "<br><br>" +
                "<button id='roll'>Кинуть кости</button>" +
                "</div>",
                showConfirmButton: false,
        });
        $('#roll').on('click', function(){
            let num_of_dices = Number($('#numberofdices').val());
            let selected_facets = Number($('#facets').val());
            let throw_amount = 0;

            var dices = [];

            for (let i = 0; i < num_of_dices; i++){
                dices.push(Math.floor((Math.random() * (selected_facets + 1)) % selected_facets + 1));
                throw_amount += dices[i];
            }

            $('#rolled').remove();
            $('#modal').append(`<div id='rolled'><p>Бросок костей = ${dices}</p><p>Сумма броска: ${throw_amount}</p></div>`);
        })
    })
})