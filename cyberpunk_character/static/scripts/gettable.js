function GetCharUrl(){
    let cur_url = window.location.href;
    var number_of_border;

    // Определение границы '/' сслыки после которой идет id персонажа
    for (let i = cur_url.length - 1; cur_url[i] != '/'; i--){
        number_of_border = i;
    }

    var number_of_char = cur_url[number_of_border];

    // Определение номера персонажа
    for (let i = number_of_border + 1; i < cur_url.length; i++){
        number_of_char += cur_url[i];
    }

    return number_of_char;
};

function humanity_change(){
    var hl_tab = $("#cybernetic").find('input');
    var hl_total = 0;
    for (let j of hl_tab){
        var name_c = j.name;
        if (name_c.indexOf("cyber_hl") != -1){
            hl_total += Number(j.value);
        }
    }
    $('#humanity_lost').val(hl_total);
}

var number_of_curchar = GetCharUrl();

$(document).ready(function TableToHtml(){
    //Кибернетика
    $.ajax({
        method: "GET",
        url: `ajax_return_cybernetic/${number_of_curchar}`,
        dataType: "json",
        data: {text: "text"},
        success: function(data){
            for (let key in data){
                $('table[id="cybernetic"]').append(`<tr><td><input class="table_long_input" name="type_cyber${key}"
                                                    value=""></td><td><input class="short_input" name="cyber_hl${key}" 
                                                    type="number" value="0" min="0" max="100" step="1"></td><td><input 
                                                    class="short_input" name="cyber_cost${key}" type="number" value="0" min="0"
                                                     max="100" step="1"></td></tr>`);
                for (let k in data[key]){
                    $(`input[name="${k}"]`).val(data[key][k]);
                }
            }
            $('tr[id="hat"]').appendTo($('table[id="cybernetic"]'));
            humanity_change();
            console.log("таблица кибернетики выведена");
        }
    })
    //В случае изменения человечности
    $('#cybernetic').on('change', humanity_change)
    //ajax_return_life_path
    $.ajax({
        method: "GET",
        url: `ajax_return_life_path/${number_of_curchar}`,
        dataType: "json",
        data: {text: "text"},
        success: function(data){
            //console.log(data);
            for (let key in data){
                $('table[id="life_path"]').append(`<tr><td><input class="year_input" value="0" name="event_year${key}"
                                                   type="number"></td><td><input class="table_long_input" value=""
                                                    name="event${key}"></td></tr>`);
                for (let k in data[key]){
                    $(`input[name="${k}"]`).val(data[key][k]);
                }
            }
            console.log("таблица событий в жизни выведена");
        }
    })
    //ajax_return_gears
    $.ajax({
        method: "GET",
        url: `ajax_return_gears/${number_of_curchar}`,
        dataType: "json",
        data: {text: "text"},
        success: function(data){
            for (let key in data){
                $('table[id="gear"]').append(`<tr><td><input class="table_long_input" name="type_gear${key}"
                                              value="0"></td><td><input class="short_input" name="gear_hl${key}"
                                              type="number" value="0" min="0" max="100" step="1"></td><td><input
                                              class="short_input" name="gear_cost${key}" type="number" value="0" min="0"
                                              max="100" step="1"></td></tr>`);
                for (let k in data[key]){
                    $(`input[name="${k}"]`).val(data[key][k]);
                }
            }
            console.log("таблица снаряжения выведена");
        }
    })
    //ajax_return_weapons
    $.ajax({
        method: "GET",
        url: `ajax_return_weapons/${number_of_curchar}`,
        dataType: "json",
        data: {text: "text"},
        success: function(data){
            //console.log(data);
            for (let key in data){
                $('table[id="weapon"]').append(`<tr><td><input name="weapon_name${key}" class="table_long_input"
                                                value=""></td><td><input name="type_weapon${key}" class="short_input"
                                                type="text" value=""></td><td><input name="accuracy${key}" class="short_input"
                                                type="number" value="" min="0" max="100" step="1"></td><td><input name="conc${key}"
                                                class="short_input" type="number" value="" min="0" max="100" step="1"></td><td><input
                                                name="avall${key}" class="short_input" type="number" value=""  min="0" max="100"
                                                step="1"></td><td><input name="dam${key}" class="short_input" type="number" value=""
                                                min="0" max="100" step="1"></td><td><input name="shells${key}" class="short_input"
                                                type="number" value=""  min="0" max="100" step="1"></td><td><input name="pace${key}"
                                                class="short_input" type="number" value="" min="0" max="100" step="1"></td><td><input
                                                name="reliability${key}" class="short_input" type="number"  value="" min="0" max="100"
                                                step="1"></td></tr>`);
                for (let k in data[key]){
                    $(`input[name="${k}"]`).val(data[key][k]);
                }
            }
            console.log("таблица оружия выведена");
        }
    })
})
