from django.contrib import admin

from .models import Character, Cybernetics, Life_events, Gear, Weapon
# Register your models here.

admin.site.register(Character)
admin.site.register(Cybernetics)
admin.site.register(Life_events)
admin.site.register(Gear)
admin.site.register(Weapon)