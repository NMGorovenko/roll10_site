from django.urls import path, include
from . import views


# принимается функция home 
# из views в текущем каталоге
urlpatterns = [
    path('', views.HomeViews, name='home'),
]